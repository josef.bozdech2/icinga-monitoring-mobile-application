import { PrismaClient, ClientConfig } from '@prisma/client';
import { HttpException } from '@exceptions/HttpException';
import { isEmpty } from '@utils/util';
import { CreateClientConfigDto } from '@/dtos/client-config.dto';

class ClientConfigService {
  public clientConfigs = new PrismaClient().clientConfig;
  /**
   *
   * @returns all existing users
   */
  public async findAll(): Promise<ClientConfig[]> {
    const allUser: ClientConfig[] = await this.clientConfigs.findMany();
    return allUser;
  }
  /**
   *
   * @param clientToken unique user token
   * @returns existing user
   */
  public async findById(clientToken: string): Promise<ClientConfig> {
    if (isEmpty(clientToken)) throw new HttpException(400, 'ClientToken is not defined');

    const findUser: ClientConfig = await this.clientConfigs.findUnique({ where: { clientToken } });

    return findUser;
  }
  /**
   *
   * @param clientToken unique user token
   * @param clientConfigData user configuration
   * @returns created user object
   */
  public async create(clientToken: string, clientConfigData: ClientConfig): Promise<ClientConfig> {
    if (isEmpty(clientConfigData)) throw new HttpException(400, 'ClientConfigData is not defined');
    const data: ClientConfig = { ...clientConfigData, clientToken };
    const createUserData: ClientConfig = await this.clientConfigs.create({
      data,
    });
    return createUserData;
  }
  /**
   *
   * @param clientToken unique user token
   * @param clientConfigData new user configuration
   * @returns updated user object
   */
  public async update(clientToken: string, clientConfigData: Partial<CreateClientConfigDto>): Promise<ClientConfig> {
    if (isEmpty(clientConfigData)) throw new HttpException(400, 'ClientConfigData is not defined');

    const findUser: ClientConfig = await this.clientConfigs.findUnique({ where: { clientToken } });
    if (!findUser) throw new HttpException(409, 'ClientToken was not found');

    const updateUserData = await this.clientConfigs.update({ where: { clientToken }, data: clientConfigData });
    return updateUserData;
  }
  /**
   *
   * @param clientToken unique user token
   * @returns removed user object
   */
  public async delete(clientToken: string): Promise<ClientConfig> {
    if (isEmpty(clientToken)) throw new HttpException(400, 'ClientToken is not defined');

    const findUser: ClientConfig = await this.clientConfigs.findUnique({ where: { clientToken } });
    if (!findUser) throw new HttpException(409, 'ClientToken was not found');

    const deleteUserData = await this.clientConfigs.delete({ where: { clientToken } });
    return deleteUserData;
  }
  /**
   *
   * @param clientToken unique user token
   * @param clientConfigList user data object
   * @returns parsed client config object
   */
  public parseClientConfig(clientToken: string, clientConfigList: any): ClientConfig {
    const auth = `${clientConfigList.username}:${clientConfigList.password}`;

    const parsedClientConfigList: ClientConfig = {
      clientToken,
      host: String(clientConfigList.host),
      port: Number(clientConfigList.port),
      path: this.getPath(clientToken, clientConfigList),
      notificationFrom: clientConfigList?.notificationTimeInterval && clientConfigList?.notificationTimeInterval[0],
      notificationTo: clientConfigList?.notificationTimeInterval && clientConfigList?.notificationTimeInterval[1],
      notificationEnable: clientConfigList.notificationEnable || false,
      auth,
    };

    return parsedClientConfigList;
  }
  /**
   *
   * @param clientToken unique user token
   * @param clientConfigList user data object
   * @returns icinga event stream substring
   */
  private getPath(clientToken: string, clientConfigList) {
    const eventTypes = [];
    const _regex = /\[([^\][]*)]/;
    const _queue = _regex.exec(clientToken);
    if (!_queue['1']) console.error('Could not extract queue id from clientToken => connection-options-parser > getPath');
    let path = `/v1/events?queue=${_queue['1']}`;

    Object.keys(clientConfigList).forEach(key => {
      if (key.includes('event_') && clientConfigList[key]) eventTypes.push(key.replace('event_', '').replace(`\t`, ''));
    });

    if (!eventTypes.length) return null;

    eventTypes.forEach(type => {
      path += '&types=' + type;
    });

    return path;
  }
}

export default ClientConfigService;
