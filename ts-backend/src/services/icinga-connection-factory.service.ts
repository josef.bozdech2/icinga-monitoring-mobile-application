import { ExpoNotification } from '@/interfaces/notification.interface';
import NotificationQueueService from './notification-queue.service';
import https from 'https';
import { HttpsAgent as Agent } from 'agentkeepalive';

class IcingaConnectionFactory {
  private clientToken: string;
  private options: Object;
  private keepAliveAgent = new Agent({
    maxSockets: 1,
    maxFreeSockets: 1,
    freeSocketTimeout: 60000,
    rejectUnauthorized: false,
    keepAlive: false,
  });
  private notificationQueueService: NotificationQueueService;
  /**
   *
   * @param clientToken unique user token
   * @param options user config option object
   * @param notificationQueueService notification queue service class instance
   */
  constructor(clientToken, options, notificationQueueService) {
    this.clientToken = clientToken;
    this.options = {
      ...options,
      method: 'POST',
      agent: this.keepAliveAgent,
      headers: {
        Accept: 'application/json',
      },
    };
    this.notificationQueueService = notificationQueueService;
  }
  /**
   * create icinga event stream api request
   */
  makeRequest() {
    try {
      const req = https.request(this.options, res => {
        res.setEncoding('utf8');
        res.on('data', chunk => {
          try {
            if (chunk === `\n`) return;
            const parsedResponse = JSON.parse(chunk);
            const _parsedNotification = this.parseNotification(parsedResponse);
            if (!_parsedNotification) return;

            this.notificationQueueService.scheduleNotification({
              to: this.clientToken,
              id: parsedResponse.timestamp,
              ..._parsedNotification,
            });
          } catch (error) {
            // FIXME: Icinga is retarded so...
            // console.error(error);
            return;
          }
        });
      });
      req.on('error', () => {
        // FIXME: Icinga is retarded so...
        // console.error(error);
        return;
      });
      req.end();
    } catch (error) {
      console.log(error);
    }
  }
  /**
   *
   * @param notification notification retrieved from icinga event stream
   * @returns parsed notification parsed to be sent to user
   */
  parseNotification(notification: ExpoNotification): Partial<ExpoNotification> {
    try {
      if (notification.type === 'CheckResult') {
        const CheckResult = {
          title: `${notification.host ? notification.host : 'Unknown host'}`,
          body: `${notification.check_result.output ? notification.check_result.output : ''}`,
        };
        return CheckResult;
      }

      if (notification.type === 'StateChange') {
        const StateChange = {
          title: 'State Change',
          body: `${notification.host ? notification.host : 'Unknown host'}${notification.service ? `.${notification.service}` : ''}:${
            notification.check_result.output ? notification.check_result.output : ''
          }`,
        };
        return StateChange;
      }

      if (notification.type === 'AcknowledgementSet') {
        const AcknowledgementSet = {
          title: 'Acknowledgement Set',
          body: `${notification.host ? notification.host : 'Unknown host'}${notification.service ? `.${notification.service}` : ''}:${
            notification.comment ? notification.comment : ''
          }`,
        };
        return AcknowledgementSet;
      }

      if (notification.type === 'AcknowledgementCleared') {
        const AcknowledgementCleared = {
          title: 'Acknowledgement Cleared',
          body: `${notification.host ? notification.host : 'Unknown host'}
              ${notification.service ? `.${notification.service}` : ''}`,
        };
        return AcknowledgementCleared;
      }

      if (notification.type === 'CommentAdded') {
        const CommentAdded = {
          title: 'Comment Added',
          body: `${notification.comment.host_name ? notification.comment.host_name : 'Unknown host'}${
            notification.comment.service_name ? `.${notification.comment.service_name}` : ''
          }:${notification.comment.text ? notification.comment.text : ''}`,
        };
        return CommentAdded;
      }

      if (notification.type === 'CommentRemoved') {
        const CommentRemoved = {
          title: 'Comment Removed',
          body: `${notification.comment.host_name ? notification.comment.host_name : 'Unknown host'}${
            notification.comment.service_name ? `.${notification.comment.service_name}` : ''
          }:${notification.comment.text ? notification.comment.text : ''}`,
        };
        return CommentRemoved;
      }

      if (notification.type === 'DowntimeAdded') {
        const DowntimeAdded = {
          title: 'Downtime Added',
          body: `${notification.downtime.host_name ? notification.downtime.host_name : 'Unknown host'}${
            notification.downtime.service_name ? `.${notification.downtime.service_name}` : ''
          }:${notification.downtime.author ? notification.downtime.author : ''}:${
            notification.downtime.comment ? notification.downtime.comment : ''
          }`,
        };
        // TODO: console.log({ from: moment(from).format('LLL'), to: moment(to).format('LLL') });

        return DowntimeAdded;
      }

      if (notification.type === 'DowntimeRemoved') {
        const DowntimeRemoved = {
          title: 'Downtime Removed',
          body: `${notification.downtime.host_name ? notification.downtime.host_name : 'Unknown host'}${
            notification.downtime.service_name ? `.${notification.downtime.service_name}` : ''
          }:${notification.downtime.author ? notification.downtime.author : ''}:${
            notification.downtime.comment ? notification.downtime.comment : ''
          }`,
        };
        return DowntimeRemoved;
      }

      if (notification.type === 'DowntimeStarted') {
        const DowntimeStarted = {
          title: 'Downtime Started',
          body: `${notification.downtime.host_name ? notification.downtime.host_name : 'Unknown host'}${
            notification.downtime.service_name ? `.${notification.downtime.service_name}` : ''
          }:${notification.downtime.author ? notification.downtime.author : ''}:${
            notification.downtime.comment ? notification.downtime.comment : ''
          }`,
        };
        return DowntimeStarted;
      }

      if (notification.type === 'DowntimeTriggered') {
        const DowntimeTriggered = {
          title: 'Downtime Triggered',
          body: `${notification.downtime.host_name ? notification.downtime.host_name : 'Unknown host'}${
            notification.downtime.service_name ? `.${notification.downtime.service_name}` : ''
          }:${notification.downtime.author ? notification.downtime.author : ''}:${
            notification.downtime.comment ? notification.downtime.comment : ''
          }`,
        };
        return DowntimeTriggered;
      }

      // FIXME:
      // throw new Error('IcingaConnectionFactory -> parseNotification -> Notification type not recognized or defined');
      return null;
    } catch (error) {
      console.error(error);
    }
  }
}

export default IcingaConnectionFactory;
