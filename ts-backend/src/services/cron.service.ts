import cron from 'node-cron';
import moment, { MomentInput } from 'moment';
import { PrismaClient, ClientConfig } from '@prisma/client';

import NotificationQueueService from './notification-queue.service';
import IcingaConnectionFactory from './icinga-connection-factory.service';
class CronService {
  public clientConfigList = new PrismaClient().clientConfig;
  public notificationQueueService = new NotificationQueueService();
  /**
   *
   * @param notificationQueueInterval - interval in which notifications are dispatched
   */
  public async init(notificationQueueInterval: number): Promise<void> {
    setInterval((): void => {
      this.notificationQueueService.pushScheduledNotifications();
    }, notificationQueueInterval);

    cron.schedule('* * * * *', async () => {
      const configList: ClientConfig[] = await this.clientConfigList.findMany();

      configList.forEach(config => {
        const [from, to] = this.getTimeInterval(config.notificationFrom, config.notificationTo);

        if (!moment().isBetween(from, to)) {
          return;
        }

        const icingaConnection = new IcingaConnectionFactory(config.clientToken, config, this.notificationQueueService);
        icingaConnection.makeRequest();
      });
    });
  }

  /**
   *
   * @param notificationFrom - interval start as Moment instance
   * @param notificationTo - interval end as Moment instance
   * @returns array of start and end times
   */
  private getTimeInterval(notificationFrom: MomentInput, notificationTo: MomentInput) {
    const tmFrom = moment(notificationFrom);
    const tmTo = moment(notificationTo);
    const from = moment().set('hours', tmFrom.get('hours')).set('minutes', tmFrom.get('minutes'));
    let to = moment().set('hours', tmTo.get('hours')).set('minutes', tmTo.get('minutes'));
    if (moment(from).isAfter(to)) to = moment(to).add(1, 'day');
    console.log({ from: moment(from).format('LLL'), to: moment(to).format('LLL') });

    return [from, to];
  }
}

export default CronService;
