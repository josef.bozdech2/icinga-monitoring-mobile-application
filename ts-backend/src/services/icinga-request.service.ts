import { PrismaClient, ClientConfig } from '@prisma/client';
import axios from 'axios';
import https from 'https';

class IcingaRequestService {
  public clientConfigList = new PrismaClient().clientConfig;
  /**
   *
   * @param expoPushToken unique user token
   * @param path icinga api endpoint
   * @param body body of api request
   * @param isPost request defining variable
   * @returns icinga api response
   */
  public async buildIcingaUrl(expoPushToken: string, path: string, body?: any, isPost?: boolean) {
    const _config: ClientConfig = await this.clientConfigList.findUnique({ where: { clientToken: expoPushToken } });

    try {
      const data = `${_config.auth}`;
      const buff = Buffer.from(data);
      const base64data = buff.toString('base64');
      axios.defaults.headers.common['Accept'] = 'application/json';
      axios.defaults.headers.common['Authorization'] = `Basic ${base64data}`;
      if (!isPost) {
        if (!body) {
          console.log("Testing A");
          const _data = await axios.get(`https://${_config.host}:${_config.port}${path}`, {
            withCredentials: true,
            headers: {
              Authorization: `Basic ${base64data}`,
            },
          });
          return _data.data;
        } else {
          console.log("Testing B");
          const _data = await axios.get(`https://${_config.host}:${_config.port}${path}`, {
            withCredentials: true,
            headers: {
              Authorization: `Basic ${base64data}`,
              'Content-type': 'application/json',
            },
            params: body,
          });
          return _data.data;
        }
      } else {
        console.log("Testing C");
        const _data = await axios.post(`https://${_config.host}:${_config.port}${path}`, {
          withCredentials: true,
          headers: {
            Authorization: `Basic ${base64data}`,
            Accept: 'application/json',
          },
          author: _config.host,
          ...body,
        });
        return _data.data;
      }
    } catch (error) {
      console.log({ error });
    }
  }
  /**
   * @deprecated
   * @param expoPushToken
   * @param path
   * @param body
   * @returns
   */
  public async buildIcingaUrlGetToPost(expoPushToken: string, path: string, body: any) {
    const _config: ClientConfig = await this.clientConfigList.findUnique({ where: { clientToken: expoPushToken } });

    try {
      const data = `${_config.auth}`;
      const buff = Buffer.from(data);
      const base64data = buff.toString('base64');
      if (!body) {
        const _data = await axios.get(`https://${_config.host}:${_config.port}${path}`, {
          withCredentials: true,
          headers: {
            Authorization: `Basic ${base64data}`,
          },
        });
        return _data.data;
      } else {
        const _data = await axios.post(`https://${_config.host}:${_config.port}${path}`, {
          withCredentials: true,
          headers: {
            'X-HTTP-Method-Override': 'GET',
            Authorization: `Basic ${base64data}`,
          },
          body: {
            ...body,
          },
        });
        return _data.data;
      }
    } catch (error) {
      console.log({ error });
    }
  }
  /**
   *  @deprecated
   * @param expoPushToken
   * @param path
   * @param body
   * @returns
   */
  public async httpPost(expoPushToken: string, path: string, body: any) {
    const config: ClientConfig = await this.clientConfigList.findUnique({ where: { clientToken: expoPushToken } });

    function _post(_config) {
      return new Promise((resolve, reject) => {
        const data = JSON.stringify(body);
        const buff = Buffer.from(`${_config.auth}`);
        const base64data = buff.toString('base64');
        const options = {
          hostname: _config.host,
          port: _config.port,
          path: path,
          method: 'POST',
          headers: {
            Authorization: `Basic ${base64data}`,
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Content-Length': data.length,
            'X-HTTP-Method-Override': 'GET',
          },
        };

        const req = https.request(options, res => {
          console.log(`statusCode: ${res.statusCode}`);
          console.log(res);
          res.on('data', d => {
            resolve(d);
          });
        });

        req.on('error', error => {
          console.error(error);
          reject(error);
        });

        req.write(data);
        req.end();
      });
    }

    _post(config);
  }
}

export default IcingaRequestService;
