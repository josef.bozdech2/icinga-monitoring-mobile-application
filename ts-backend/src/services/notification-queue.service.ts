import { ClientConfig, PrismaClient } from '.prisma/client';
import { ExpoNotification } from '@/interfaces/notification.interface';
import { logger } from '@utils/logger';
import { Expo } from 'expo-server-sdk';

class NotificationQueueService {
  private expo = new Expo();
  private queue: Array<ExpoNotification> = [];
  private notificationIdQueue = [];
  private clientConfigs = new PrismaClient().clientConfig;
  /**
   *
   * @param _id unique notification identifier
   * @param _token unique user token
   * @returns return true if unique, else false
   */
  verifyNotificationDuplicity(_id: number, _token: string | string[]) {
    if (this.notificationIdQueue.includes(`${_id}${_token}`)) {
      return false;
    }
    this.notificationIdQueue.push(`${_id}${_token}`);
    if (this.notificationIdQueue.length > 1000) {
      this.notificationIdQueue.shift();
    }
    return true;
  }
  /**
   *
   * @param _notification notification object
   * @returns null
   */
  scheduleNotification(_notification: ExpoNotification) {
    if (!this.verifyNotificationDuplicity(_notification.id, _notification.to)) return;
    this.queue.push({
      sound: 'default',
      ..._notification,
    });
  }
  /**
   * Dispatch notifications to ExpoNotificationService
   */
  async pushScheduledNotifications() {
    const _filteredQueue: Array<ExpoNotification> = await this.filterEnabledNotifications(this.queue);
    logger.info(`========SENDING ${_filteredQueue.length} NOTIFICATIONS out of ${this.queue.length}========`);
    const chunks = this.expo.chunkPushNotifications(_filteredQueue);
    this.queue = [];
    const tickets = [];
    (async () => {
      for (const chunk of chunks) {
        try {
          const ticketChunk = await this.expo.sendPushNotificationsAsync(chunk);
          tickets.push(...ticketChunk);
        } catch (error) {
          console.error(error);
        }
      }
    })();
  }
  /**
   *
   * @param _notifications array of notification objects
   * @returns filtered array of notification objects
   */
  async filterEnabledNotifications(_notifications: Array<ExpoNotification>): Promise<Array<ExpoNotification>> {
    const _users: Array<ClientConfig> = await await this.clientConfigs.findMany({ where: { notificationEnable: true } });
    const _userTokenList: Array<String> = _users.map(user => user.clientToken);
    return _notifications.filter(notification => {
      return _userTokenList.includes(String(notification.to));
    });
  }
}

export default NotificationQueueService;
