import { IsInt, IsString } from 'class-validator';

export class CreateClientConfigDto {
  @IsString()
  public clientToken: string;

  @IsString()
  public host: string;

  @IsInt()
  public port: number;

  @IsString()
  public path: string;

  @IsString()
  public notificationFrom: string;

  @IsString()
  public notificationTo: string;

  @IsString()
  public auth: string;

  @IsString()
  public notificationEnable: boolean;
}
