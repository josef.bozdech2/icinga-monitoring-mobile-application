-- CreateTable
CREATE TABLE `ClientConfig` (
    `clientToken` VARCHAR(191) NOT NULL,
    `host` VARCHAR(191) NOT NULL,
    `notificationTimeInterval` INTEGER NOT NULL,

    UNIQUE INDEX `ClientConfig.host_unique`(`host`),
    PRIMARY KEY (`clientToken`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
