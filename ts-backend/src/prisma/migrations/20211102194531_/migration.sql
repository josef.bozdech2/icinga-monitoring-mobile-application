/*
  Warnings:

  - You are about to drop the column `notificationTimeInterval` on the `ClientConfig` table. All the data in the column will be lost.
  - Added the required column `path` to the `ClientConfig` table without a default value. This is not possible if the table is not empty.
  - Added the required column `port` to the `ClientConfig` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE `ClientConfig` DROP COLUMN `notificationTimeInterval`,
    ADD COLUMN `notificationFrom` VARCHAR(191),
    ADD COLUMN `notificationTo` VARCHAR(191),
    ADD COLUMN `path` VARCHAR(191) NOT NULL,
    ADD COLUMN `port` INTEGER NOT NULL,
    MODIFY `notificationEnable` BOOLEAN;
