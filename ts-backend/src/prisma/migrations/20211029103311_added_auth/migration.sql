/*
  Warnings:

  - Added the required column `auth` to the `ClientConfig` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE `ClientConfig` ADD COLUMN `auth` VARCHAR(191) NOT NULL;
