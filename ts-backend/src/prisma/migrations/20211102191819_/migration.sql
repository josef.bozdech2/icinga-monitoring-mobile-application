/*
  Warnings:

  - Added the required column `notificationEnable` to the `ClientConfig` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE `ClientConfig` ADD COLUMN `notificationEnable` BOOLEAN NOT NULL;
