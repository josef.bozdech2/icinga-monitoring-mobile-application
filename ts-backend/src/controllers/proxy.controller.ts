import { NextFunction, Request, Response } from 'express';

import IcingaRequestService from '@services/icinga-request.service';
import { HttpException } from '@/exceptions/HttpException';

class ProxyController {
  public icingaRequestService = new IcingaRequestService();

  //#region ServiceEntity
  /**
   *
   * @param req - request param
   * @param res - response param
   * @param next - next function
   */
  public getServiceList = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const _params = { ...req.query };
      const expoPushToken = String(req.query.expoPushToken);
      if (!expoPushToken) throw new HttpException(400, 'expoPushToken is not defined');

      if (_params)
        res.status(200).json(
          await this.icingaRequestService.buildIcingaUrl(
            expoPushToken,
            `/v1/objects/services?attrs=display_name&attrs=active&attrs=last_check&attrs=last_check_result&attrs=state`,
            {
              filter: `match(\"${_params.host ? _params.host : ''}*\", host.name)`,
            },
            false,
          ),
        );
      else
        res
          .status(200)
          .json(
            await this.icingaRequestService.buildIcingaUrl(
              expoPushToken,
              '/v1/objects/services?attrs=display_name&attrs=active&attrs=last_check_result&attrs=last_check&attrs=state',
              null,
              false,
            ),
          );
    } catch (error) {
      next(error);
    }
  };
  /**
   *
   * @param req - request param
   * @param res - response param
   * @param next - next function
   */

  public getServiceDetail = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const expoPushToken = String(req.query.expoPushToken);
      if (!expoPushToken) throw new HttpException(400, 'expoPushToken is not defined');
      const serviceName = req.query.serviceId;

      const _serviceArray = await this.icingaRequestService.buildIcingaUrl(expoPushToken, '/v1/objects/services/' + serviceName, null, false);
      const serviceDetail = _serviceArray.results[0];

      res.status(200).json(serviceDetail);
    } catch (error) {
      next(error);
    }
  };
  //#endregion ServiceEntity

  //#region HostEntity
  /**
   *
   * @param req - request param
   * @param res - response param
   * @param next - next function
   */
  public getHostList = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const _params = { ...req.query };
      const expoPushToken = String(req.query.expoPushToken);
      if (!expoPushToken) throw new HttpException(400, 'expoPushToken is not defined');
      if (_params)
        res
          .status(200)
          .json(
            await this.icingaRequestService.buildIcingaUrl(
              expoPushToken,
              `/v1/objects/hosts?attrs=display_name&attrs=active&attrs=last_check&attrs=last_check_result&attrs=state&filter=match(\"*${
                _params.host ? _params.host : ''
              }*\", host.name)`,
            ),
          );
      else
        res
          .status(200)
          .json(
            await this.icingaRequestService.buildIcingaUrl(
              expoPushToken,
              '/v1/objects/hosts?attrs=display_name&attrs=active&attrs=last_check&attrs=last_check_result&attrs=state',
              null,
              false,
            ),
          );
    } catch (error) {
      next(error);
    }
  };
  /**
   *
   * @param req - request param
   * @param res - response param
   * @param next - next function
   */
  public getHostDetail = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const expoPushToken = String(req.query.expoPushToken);
      if (!expoPushToken) throw new HttpException(400, 'expoPushToken is not defined');
      const hostName = req.query.hostId;

      const _hostArray = await this.icingaRequestService.buildIcingaUrl(expoPushToken, '/v1/objects/hosts/' + hostName, null, false);
      const hostDetail = _hostArray.results[0];

      res.status(200).json(hostDetail);
    } catch (error) {
      next(error);
    }
  };
  //#endregion HostEntity

  //#region OverviewEntity
  /**
   *
   * @param req - request param
   * @param res - response param
   * @param next - next function
   */
  public getOverviewList = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const expoPushToken = String(req.query.expoPushToken);
      if (!expoPushToken) throw new HttpException(400, 'expoPushToken is not defined');

      const serviceList = await this.icingaRequestService.buildIcingaUrl(
        expoPushToken,
        '/v1/objects/services?attrs=display_name&attrs=check_command&joins=host.address&filter=service.state==2',
        null,
      );
      const hostList = await this.icingaRequestService.buildIcingaUrl(expoPushToken, '/v1/objects/hosts?filter=host.state==1.0', null, false);
      res.status(200).json({ serviceList, hostList });
    } catch (error) {
      next(error);
    }
  };
  //#endregion OverviewEntity

  //#region Objects
  /**
   *
   * @param req - request param
   * @param res - response param
   * @param next - next function
   */
  public getComments = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const expoPushToken = String(req.query.expoPushToken);
      if (!expoPushToken) throw new HttpException(400, 'expoPushToken is not defined');

      const comments = await this.icingaRequestService.buildIcingaUrl(
        expoPushToken,
        `/v1/objects/comments?filter=${req.query.filter}&type=${req.query.type}`,
        null,
      );
      res.status(200).json(comments);
    } catch (error) {
      next(error);
    }
  };

  /**
   *
   * @param req - request param
   * @param res - response param
   * @param next - next function
   */
  public getDowntimes = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const expoPushToken = String(req.query.expoPushToken);
      if (!expoPushToken) throw new HttpException(400, 'expoPushToken is not defined');

      const downtimes = await this.icingaRequestService.buildIcingaUrl(
        expoPushToken,
        `/v1/objects/downtimes?filter=${req.query.filter}&type=${req.query.type}`,
        null,
      );
      res.status(200).json(downtimes);
    } catch (error) {
      next(error);
    }
  };
  //#endregion Objects

  //#region Actions

  /**
   *
   * @param req - request param
   * @param res - response param
   * @param next - next function
   */
  public postAcknowledgeProblem = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const expoPushToken = String(req.body.expoPushToken);
    if (!expoPushToken) throw new HttpException(400, 'expoPushToken is not defined');
    const _body = req.body.params;
    try {
      const acknowledgeProblem = await this.icingaRequestService.buildIcingaUrl(expoPushToken, '/v1/actions/acknowledge-problem', _body, true);
      res.status(200).send(acknowledgeProblem);
    } catch (error) {
      next(error);
    }
  };
  /**
   *
   * @param req - request param
   * @param res - response param
   * @param next - next function
   */
  public postRemoveAcknowledgement = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const expoPushToken = String(req.body.expoPushToken);
    if (!expoPushToken) throw new HttpException(400, 'expoPushToken is not defined');
    const _body = req.body.params;
    try {
      const removeAck = await this.icingaRequestService.buildIcingaUrl(expoPushToken, '/v1/actions/remove-acknowledgement', _body, true);
      res.status(200).send(removeAck);
    } catch (error) {
      next(error);
    }
  };
  /**
   *
   * @param req - request param
   * @param res - response param
   * @param next - next function
   */
  public postScheduleDowntime = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const expoPushToken = String(req.body.expoPushToken);
    if (!expoPushToken) throw new HttpException(400, 'expoPushToken is not defined');
    const _body = req.body.params;
    try {
      const scheduleDowntime = await this.icingaRequestService.buildIcingaUrl(expoPushToken, '/v1/actions/schedule-downtime', _body, true);
      res.status(200).send(scheduleDowntime);
    } catch (error) {
      next(error);
    }
  };
  /**
   *
   * @param req - request param
   * @param res - response param
   * @param next - next function
   */
  public postRemoveDowntime = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const expoPushToken = String(req.body.expoPushToken);
    if (!expoPushToken) throw new HttpException(400, 'expoPushToken is not defined');
    const _body = { downtime: req.body.downtime };
    try {
      const removeDowntime = await this.icingaRequestService.buildIcingaUrl(expoPushToken, '/v1/actions/remove-downtime', _body, true);
      res.status(200).send(removeDowntime);
    } catch (error) {
      next(error);
    }
  };
  /**
   *
   * @param req - request param
   * @param res - response param
   * @param next - next function
   */
  public postAddComment = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const expoPushToken = String(req.body.expoPushToken);
    if (!expoPushToken) throw new HttpException(400, 'expoPushToken is not defined');

    const _body = { ...req.body.params };

    try {
      const addComment = await this.icingaRequestService.buildIcingaUrl(expoPushToken, '/v1/actions/add-comment', _body, true);
      res.status(200).send(addComment);
    } catch (error) {
      next(error);
    }
  };
  /**
   *
   * @param req - request param
   * @param res - response param
   * @param next - next function
   */
  public postRemoveComment = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const expoPushToken = String(req.body.expoPushToken);
    if (!expoPushToken) throw new HttpException(400, 'expoPushToken is not defined');
    const _body = { comment: req.body.comment };

    try {
      const removeComment = await this.icingaRequestService.buildIcingaUrl(expoPushToken, '/v1/actions/remove-comment', _body, true);
      res.status(200).send(removeComment);
    } catch (error) {
      next(error);
    }
  };
  /**
   *
   * @param req - request param
   * @param res - response param
   * @param next - next function
   */
  public postRescheduleCheck = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const expoPushToken = String(req.body.expoPushToken);
    if (!expoPushToken) throw new HttpException(400, 'expoPushToken is not defined');
    try {
      const rescheduleCheck = await this.icingaRequestService.buildIcingaUrl(expoPushToken, '/v1/actions/remove-comment', req.body.params, true);
      res.status(200).send(rescheduleCheck);
    } catch (error) {
      next(error);
    }
  };
  /**
   *
   * @param req - request param
   * @param res - response param
   * @param next - next function
   */
  public postSendCustomNotification = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const expoPushToken = String(req.body.expoPushToken);
    if (!expoPushToken) throw new HttpException(400, 'expoPushToken is not defined');
    const _body = req.body.params;
    try {
      const sendCustomNotification = await this.icingaRequestService.buildIcingaUrl(
        expoPushToken,
        '/v1/actions/send-custom-notification',
        _body,
        true,
      );
      res.status(200).send(sendCustomNotification);
    } catch (error) {
      next(error);
    }
  };
  //#endregion Actions
}

export default ProxyController;
