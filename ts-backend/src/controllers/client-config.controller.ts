import { NextFunction, Request, Response } from 'express';
import { ClientConfig } from '@prisma/client';
import clientConfigService from '@services/client-config.service';
import { HttpException } from '@/exceptions/HttpException';

class ClientConfigController {
  public clientConfigService = new clientConfigService();
  /**
   *
   * @param req - request param
   * @param res - response param
   * @param next - next function
   */
  public getAll = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const allClientConfig: ClientConfig[] = await this.clientConfigService.findAll();

      res.status(200).json({ data: allClientConfig, message: 'findAll' });
    } catch (error) {
      next(error);
    }
  };

  /**
   *
   * @param req - request param
   * @param res - response param
   * @param next - next function
   */
  public register = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { clientToken, clientConfigList } = req.body;

      const transformedConfigList: ClientConfig = this.clientConfigService.parseClientConfig(clientToken, clientConfigList);
      const existingClientConfig: ClientConfig = await this.clientConfigService.findById(clientToken);

      if (existingClientConfig) {
        const createClientConfigData: ClientConfig = await this.clientConfigService.update(clientToken, transformedConfigList);
        res.status(200).json({ data: createClientConfigData, message: 'Client successfully registered' });
      } else {
        const updateClientConfigData: ClientConfig = await this.clientConfigService.create(clientToken, transformedConfigList);
        res.status(201).json({ data: updateClientConfigData, message: 'Client successfully updated' });
      }
    } catch (error) {
      next(error);
    }
  };
  /**
   *
   * @param req - request param
   * @param res - response param
   * @param next - next function
   */

  public manageNotificationSubscription = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { clientToken, notificationEnable } = req.body;
      const existingClientConfig: ClientConfig = await this.clientConfigService.findById(clientToken);

      if (existingClientConfig) {
        const createClientConfigData: ClientConfig = await this.clientConfigService.update(clientToken, {
          notificationEnable: Boolean(notificationEnable),
        });
        res.status(200).json({ data: createClientConfigData, message: 'Notification management state has been changed' });
      } else {
        throw new HttpException(400, 'User not found');
      }
    } catch (error) {
      next(error);
    }
  };
}

export default ClientConfigController;
