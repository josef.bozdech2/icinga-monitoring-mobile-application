export class HttpException extends Error {
  public status: number;
  public message: string;

  /**
   *
   * @param status status code
   * @param message message string
   */
  constructor(status: number, message: string) {
    super(message);
    this.status = status;
    this.message = message;
  }
}
