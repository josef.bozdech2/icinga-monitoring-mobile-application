import { Router } from 'express';
import ProxyController from '@controllers/proxy.controller';
import { Routes } from '@interfaces/routes.interface';

class UsersRoute implements Routes {
  public path = '/icingaProxy';
  public router = Router();
  public proxyController = new ProxyController();

  /**
   * initialize programmed routes
   */
  constructor() {
    this.initializeRoutes();
  }
  /**
   * bind routes to controller functions
   */
  private initializeRoutes() {
    this.router.get(`${this.path}/service/list`, this.proxyController.getServiceList);
    this.router.get(`${this.path}/service/detail`, this.proxyController.getServiceDetail);
    this.router.get(`${this.path}/host/list`, this.proxyController.getHostList);
    this.router.get(`${this.path}/host/detail`, this.proxyController.getHostDetail);
    this.router.get(`${this.path}/overview/list`, this.proxyController.getOverviewList);
    this.router.get(`${this.path}/objects/comments`, this.proxyController.getComments);
    this.router.get(`${this.path}/objects/downtimes`, this.proxyController.getDowntimes);
    this.router.post(`${this.path}/actions/addComment`, this.proxyController.postAddComment);
    this.router.post(`${this.path}/actions/removeComment`, this.proxyController.postRemoveComment);
    this.router.post(`${this.path}/actions/acknowledgeProblem`, this.proxyController.postAcknowledgeProblem);
    this.router.post(`${this.path}/actions/removeAck`, this.proxyController.postRemoveAcknowledgement);
    this.router.post(`${this.path}/actions/scheduleDowntime`, this.proxyController.postScheduleDowntime);
    this.router.post(`${this.path}/actions/removeDowntime`, this.proxyController.postRemoveDowntime);
    this.router.post(`${this.path}/actions/rescheduleCheck`, this.proxyController.postRescheduleCheck);
    this.router.post(`${this.path}/actions/sendCustomNotification`, this.proxyController.postSendCustomNotification);
  }
}

export default UsersRoute;
