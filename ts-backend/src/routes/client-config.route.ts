import ClientConfigController from '@/controllers/client-config.controller';
import { Routes } from '@interfaces/routes.interface';
import { Router } from 'express';

class ClientConfigRoute implements Routes {
  public path = '/clientConfig';
  public router = Router();
  public clientConfigController = new ClientConfigController();

  /**
   * initialize programmed routes
   */
  constructor() {
    this.initializeRoutes();
  }

  /**
   * bind routes to controller functions
   */
  private initializeRoutes() {
    this.router.get(`${this.path}`, this.clientConfigController.getAll);
    this.router.post(`${this.path}/register`, this.clientConfigController.register);
    this.router.post(`${this.path}/enable`, this.clientConfigController.manageNotificationSubscription);
  }
}

export default ClientConfigRoute;
