process.env['NODE_CONFIG_DIR'] = __dirname + '/configs';

import 'dotenv/config';
import App from '@/app';
import IndexRoute from '@routes/index.route';
import ProxyRoute from '@/routes/proxy.route';
import ClientConfigRoute from '@routes/client-config.route';
import validateEnv from '@utils/validateEnv';

validateEnv();
/**
 * Create server instance
 */
const app = new App([new IndexRoute(), new ProxyRoute(), new ClientConfigRoute()]);
/**
 * Start listening to requests
 */
app.listen();
