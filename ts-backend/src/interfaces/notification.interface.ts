import { ExpoPushMessage } from 'expo-server-sdk';

interface CheckResult {
  output: string;
}
interface Comment {
  text: string;
  author?: string;
  service_name?: string;
  host_name?: string;
}
interface Downtime {
  author: string;
  comment?: string;
  service_name?: string;
  host_name?: string;
}

export interface ExpoNotification extends ExpoPushMessage {
  id?: number;
  host?: string;
  type?: string;
  service?: string;
  comment?: Comment;
  downtime?: Downtime;
  check_result?: CheckResult;
}
