# KivIci
The KivIci application is a mobile app for Icinga2 administrators, which want to use mobile device for viewing current host and service states, making actions on them, and receiving real time notifications of ongoing host and service events. 
Currently, KivIci is only available to download from here, in the future, there may be an official release of the application on Google Play and AppStore. 

The project consists of 2 parts: mobile application and backend. While mobile app serves as browsing device for Icinga2, backend serves as a proxy and notification dispatcher for the application.

This readme contains information on how to run this application inside Expo Go application, since there is currently no official release yet.

## Frontend
### Requirements
1. Install Node.js from https://nodejs.org/en/.
2. Install Expo from https://docs.expo.dev/get-started/installation/ + Expo Go app for your device from Google Play or App Store.
3. Started backend.
### Usage
1. **npm install** inside the **frontend** directory
2. **expo start** inside the **frontend** directory
3. In your mobile device, start Expo Go app and click the project button. The application should create a bundle and start up.
### Limitations
This project currently contains my Google API key for Firebase for push notifications. If you are trying to run the application inside your environment, consider changing the **google_play_api_key.json** file to your own API key.
## Backend
### Requirements
1. Installed Docker and Docker Compose from https://docs.docker.com/get-docker/.
### Usage
1. Inside the **ts-backend** directory, run **docker-compose up**. The project should start up accordingly.
