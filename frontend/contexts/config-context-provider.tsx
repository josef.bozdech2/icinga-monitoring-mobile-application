import React, { useEffect, useState } from "react";
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Spinner } from "native-base";
import ConfigContext, { ConfigObjectType } from "./config-context";

const LOCAL_STORAGE_CONFIG_KEY = "appinga-config-list";
const LOCAL_STORAGE_ACTIVE_CONFIG_KEY = "apping-active-config";
const DEFAULT_CONFIG = {
  host: "147.228.67.28",
  port: "5665",
  username: "root",
  password: "7592",
  event_StateChange: true,
  notificationTimeInterval: ["2021-06-10T06:00:18.225Z", "2021-06-10T20:00:18.225Z"],
  notificationEnable: true,
};
/**
 * 
 * @param param0 params from encapsuled components
 * @returns ConfigContexts
 */
function ConfigContextProvider({ children }: { children: React.ReactNode}) {
  const [isLoading, setIsLoading] = useState(true);
  const [config, setConfig] = useState<ConfigObjectType>(DEFAULT_CONFIG);

  useEffect(() => {
    loadConfig();
  }, [children]);
  /**
   * Load config from local storage
   */
  async function loadConfig() {
    await AsyncStorage.getItem(LOCAL_STORAGE_ACTIVE_CONFIG_KEY);
    const _storage_config: string | null = await AsyncStorage.getItem(LOCAL_STORAGE_CONFIG_KEY);
    let _config = null;
    if ( _storage_config !== null ){
      _config = JSON.parse(_storage_config);
    }
    setConfig(_config || DEFAULT_CONFIG);
    setIsLoading(false);
  }
  /**
   * 
   * @param newConfig new configuration of Icinga2
   */
  async function handleSetConfig(newConfig: ConfigObjectType) {
    AsyncStorage.setItem(LOCAL_STORAGE_CONFIG_KEY, JSON.stringify(newConfig));
    setConfig(newConfig);
  }

  if (isLoading) return <Spinner />;

  return (
    <ConfigContext.Provider
      value={{
        config,
        loadConfig,
        setConfig: handleSetConfig,
      }}>
      {children}
    </ConfigContext.Provider>
  );
}

export default ConfigContextProvider;
