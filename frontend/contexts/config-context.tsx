import React from "react";

export type ConfigObjectType = {
  host: string;
  port: string;
  username: string;
  password: string;
  event_CheckResult?: boolean;
  event_StateChange?: boolean;
  event_AcknowledgementCleared?: boolean;
  event_CommentAdded?: boolean;
  event_CommentRemoved?: boolean;
  event_DowntimeAdded?: boolean;
  event_DowntimeRemoved?: boolean;
  event_DowntimeStarted?: boolean;
  event_DowntimeTriggered?: boolean;
  event_AcknowledgementSet?: boolean;
  event_Flapping?: boolean;
  notificationTimeInterval: string[];
  notificationEnable: boolean;
};

export type ConfigContextType = {
  config: ConfigObjectType;
  loadConfig: () => void;
  setConfig: (config: ConfigObjectType) => void;
};

const ConfigContext = React.createContext<ConfigContextType | null>(null);
export default ConfigContext;
