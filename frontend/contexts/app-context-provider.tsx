import React, { useState } from "react";
import AppContext from "./app-context";
/**
 * 
 * @param param0 passed props from encapsuled components
 * @returns AppContext
 */
export interface AppContextProviderParams {
  children: React.ReactNode;
}
function AppContextProvider({ children }: AppContextProviderParams) {
  const [isSubscribed, setIsSubscribed] = useState(false);
  const [expoPushToken, setExpoPushToken] = useState<string | null>(null);
  const [isValidOS, setIsValidOS] = useState(false);

  return (
    <AppContext.Provider
      value={{
        isSubscribed,
        setIsSubscribed,
        expoPushToken,
        setExpoPushToken,
        isValidOS,
        setIsValidOS,
      }}>
      {children}
    </AppContext.Provider>
  );
}

export default AppContextProvider;
