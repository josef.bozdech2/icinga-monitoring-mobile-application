import React from "react";

export type AppContextWrapperType = {
  isSubscribed: boolean;
  setIsSubscribed: (isSubscribed: boolean) => void;
  expoPushToken: string | null;
  setExpoPushToken: (expoPushToken: string) => void;
  isValidOS: boolean;
  setIsValidOS: (isValidOS: boolean) => void;
};

const AppContext = React.createContext<AppContextWrapperType | null>(null);
export default AppContext;
