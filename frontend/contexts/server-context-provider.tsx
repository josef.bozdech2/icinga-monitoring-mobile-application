import React, { useEffect, useState } from "react";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { Spinner } from "native-base";
import ServerContext, { ServerObjectType } from "./server-context";

const LOCAL_STORAGE_CONFIG_KEY = "appinga-server-config";
const DEFAULT_CONFIG = {};

/**
 *
 * @param param0 params from encapsuled components
 * @returns ServerContexts
 */
function ServerContextProvider({ children }: { children: React.ReactNode }) {
  const [isLoading, setIsLoading] = useState(true);
  const [serverConfig, setServer] = useState<Partial<ServerObjectType>>(DEFAULT_CONFIG);

  useEffect(() => {
    loadServer();
  }, [children]);
  /**
   * Load config from local storage
   */
  async function loadServer() {
    const _storage_config: string | null = await AsyncStorage.getItem(LOCAL_STORAGE_CONFIG_KEY);
    let _config = null;
    if (_storage_config !== null) {
      _config = JSON.parse(_storage_config);
    }
    setServer(_config || DEFAULT_CONFIG);
    setIsLoading(false);
  }
  /**
   *
   * @param newServer new configuration of Kivici server
   */
  async function handleSetServer(newServer: ServerObjectType) {
    AsyncStorage.setItem(LOCAL_STORAGE_CONFIG_KEY, JSON.stringify(newServer));
    setServer(newServer);
  }

  if (isLoading) return <Spinner />;

  return (
    <ServerContext.Provider
      value={{
        serverConfig,
        loadServer,
        setServer: handleSetServer,
      }}>
      {children}
    </ServerContext.Provider>
  );
}

export default ServerContextProvider;
