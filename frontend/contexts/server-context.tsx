import React from "react";

export type ServerObjectType = {
  host: string;
  port: string;
  username?: string;
  password?: string;
  isServerConnectionValid: boolean;
};

export type ServerContextType = {
  serverConfig: Partial<ServerObjectType>;
  loadServer: () => void;
  setServer: (config: ServerObjectType) => void;
};

const ServerContext = React.createContext<ServerContextType | null>(null);
export default ServerContext;
