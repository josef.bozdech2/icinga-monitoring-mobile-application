export const SERVICE_COLOR_ENUM = {
    0: "#388f28",
    1: "#D37506",
    2: "#c90f09",
    3: "#8155BA",
}

export const HOST_COLOR_ENUM = {
    0: "#388f28",
    1: "#c90f09",
}