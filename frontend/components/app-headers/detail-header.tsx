import { Body, Button, Header, Icon, Left, Right, Subtitle, Title } from "native-base";
import React from "react";
import { Platform } from "react-native";
import { HOST_COLOR_ENUM, SERVICE_COLOR_ENUM } from "../../enums/state-color-enum";

interface DetailHeaderProps {
  navigation: any;
  title: string;
  subtitle: string;
  status: any;
  type: string;
}
/**
 *
 * @param param0 object of required params
 * @returns detail header component
 */
export const DetailHeader = ({ navigation, title, subtitle, status, type }: DetailHeaderProps) => {
  function getBackgroundColor(_type: string, _status: any): string {
    if (_status === 4) return "#3e52b3";
    if (type === "host") return HOST_COLOR_ENUM[_status];
    return SERVICE_COLOR_ENUM[_status];
  }

  return (
    <Header style={{ backgroundColor: getBackgroundColor(type, status) }} androidStatusBarColor={getBackgroundColor(type, status)}>
      <Left>
        <Button transparent>
          <Icon name="arrow-back" onPress={() => navigation.goBack()} />
        </Button>
      </Left>
      <Body>
        <Title>{title || ""}</Title>
        {subtitle && <Subtitle style={{ color: Platform.OS === "ios" ? "black" : "white" }}>{subtitle}</Subtitle>}
      </Body>
      <Right />
    </Header>
  );
};

export default DetailHeader;
