import { Header, Icon, Subtitle, Title } from "native-base";
import React, { useContext } from "react";
import { Pressable, StyleSheet, View } from "react-native";
import AppContext from "../../contexts/app-context";
import ConfigContext from "../../contexts/config-context";
import usePlatform from "../../hooks/use-platform";

interface Props {
  navigation: any;
  title: string | React.FC;
  subtitle?: string | React.FC;
  rightToolbar?: string | React.FC | any;
}
/**
 *
 * @param param0 object of required params
 * @returns overview header component
 */
export const OverviewHeader: React.FC<Props> = ({ navigation, title, subtitle, rightToolbar }) => {
  const { isSubscribed } = useContext(AppContext)!;
  const { config } = useContext(ConfigContext)!;
  const { isIos } = usePlatform();
  const isValidConfiguration = config?.host;
  return (
    <Header>
      <View style={styles.containerStyle}>
        {/* <View style={styles.backButtonWrapperStyle}> */}
        <Pressable style={styles.backButtonWrapperStyle} onPress={() => navigation.openDrawer()}>
          <Icon style={{ color: isIos ? "#000" : "#fff" }} name="menu" />
        </Pressable>
        {/* </View> */}
        <View style={styles.screenTitleStyle}>
          <Title>{title}</Title>
          {subtitle && <Subtitle>{subtitle}</Subtitle>}
        </View>
        {rightToolbar || (
          <View style={styles.rightToolbarStyle}>
            <Icon style={{ color: isValidConfiguration ? "limegreen" : "red", height: 32 }} name="save" type="MaterialIcons" />
            <Icon style={{ color: isSubscribed ? "limegreen" : "red", height: 32 }} name="circle-notifications" type="MaterialIcons" />
          </View>
        )}
      </View>
    </Header>
  );
};

export default OverviewHeader;

const styles = StyleSheet.create({
  containerStyle: {
    display: "flex",
    flexDirection: "row",
    width: "100%",
    height: "100%",
    alignContent: "center",
    justifyContent: "center",
  },
  backButtonWrapperStyle: {
    display: "flex",
    width: 56,
    paddingLeft: 8,
    height: "100%",
    alignContent: "flex-start",
    justifyContent: "center",
  },

  screenTitleStyle: {
    display: "flex",
    alignContent: "center",
    justifyContent: "center",
    flex: 2,
  },
  rightToolbarStyle: {
    display: "flex",
    alignContent: "center",
    justifyContent: "center",
    flexDirection: "row",
    flex: 1,
    paddingTop: 12,
    height: 56,
  },
});
