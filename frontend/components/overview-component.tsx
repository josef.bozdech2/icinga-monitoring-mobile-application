import { useNavigation } from "@react-navigation/native";
import { Body, Card, CardItem, Content, Icon, Left, List, ListItem, Spinner, Text } from "native-base";
import React, { useContext } from "react";
import styled from "styled-components";
import AppContext from "../contexts/app-context";
import ConfigContext from "../contexts/config-context";
import useData from "../hooks/use-data";
import { RowObject } from "../interfaces/icinga.interface";
import { AuthStackScreenProps, AuthStackScreens } from "../navigators/auth-stack/types";
import { OverviewStackScreens } from "../navigators/overview-stack/types";
import RefreshControllerComponent from "./refresh-controller-component";

type OverviewScreenProps = AuthStackScreenProps<AuthStackScreens.OverviewScreen>;

const OverviewComponent = () => {
  const appContext = useContext(AppContext);
  const { config } = useContext(ConfigContext)!;
  //TODO: Is this ok?
  const navigation = useNavigation();
  const { data, isLoading, onRefresh } = useData({
    config,
    url: "/icingaProxy/overview/list",
    dtoIn: { expoPushToken: appContext?.expoPushToken },
    refreshInterval: 30000,
  });

  if (isLoading) return <Spinner />;

  const _hostList = data?.hostList?.results?.slice(0, 10);
  const _serviceList = data?.serviceList?.results?.slice(0, 10);

  return (
    <RefreshControllerComponent onRefresh={onRefresh}>
      <Card>
        <CardItem header>
          <HeaderWrapper>
            <Badge>
              <BadgeText>{data?.hostList?.results?.length || "0"}</BadgeText>
            </Badge>
            <Text style={{ fontWeight: "bold" }} onPress={() => navigation.navigate(OverviewStackScreens.HostList)}>
              Host Problem
            </Text>
          </HeaderWrapper>
        </CardItem>
        {/* <CardItem>
          <Text>{_hostList?.length}</Text>
        </CardItem> */}
        {_hostList?.length !== 0 && (
          <CardItem>
            <Content style={{ marginTop: -10 }}>
              <List>
                {_hostList?.map((item: RowObject) => {
                  return (
                    <ListItem
                      icon
                      key={item.attrs.display_name}
                      onPress={() => navigation.navigate(OverviewStackScreens.HostDetail, { rowId: item.name })}>
                      <Left>
                        <Icon style={{ fontSize: 24, color: "red", left: -17 }} name="remove-circle" type="MaterialIcons" />
                      </Left>
                      <Body style={{ paddingBottom: 4 }}>
                        <Text>{item.attrs.display_name}</Text>
                        <Text numberOfLines={1} style={{ overflow: "hidden" }} note>
                          {item.attrs?.last_check_result?.output || "UNKNOWN STATE"}
                        </Text>
                      </Body>
                    </ListItem>
                  );
                })}
              </List>
            </Content>
          </CardItem>
        )}
        {data?.hostList?.results?.length > 10 && (
          <CardItem>
            <Text style={{ fontWeight: "bold", width: "100%" }} onPress={() => navigation.navigate(OverviewStackScreens.HostList)}>
              Show more ...
            </Text>
          </CardItem>
        )}
      </Card>
      <Card>
        <CardItem header>
          <HeaderWrapper>
            <Badge>
              <BadgeText>{data?.serviceList?.results?.length || "0"}</BadgeText>
            </Badge>
            <Text style={{ fontWeight: "bold" }} onPress={() => navigation.navigate(OverviewStackScreens.ServiceList)}>
              Service Problem
            </Text>
          </HeaderWrapper>
        </CardItem>
        {_serviceList?.length && (
          <CardItem>
            <Content style={{ marginTop: -10 }}>
              <List>
                {_serviceList?.map((item: RowObject) => {
                  return (
                    <ListItem
                      icon
                      key={item.name}
                      onPress={() => navigation.navigate(OverviewStackScreens.ServiceDetail, { rowId: item.name })}>
                      <Left>
                        <Icon style={{ fontSize: 24, color: "red", left: -17 }} name="remove-circle" type="MaterialIcons" />
                      </Left>
                      <Body style={{ paddingBottom: 4 }}>
                        <Text numberOfLines={1} style={{ overflow: "hidden" }}>
                          {item.name}
                        </Text>
                        <Text note numberOfLines={1} style={{ overflow: "hidden" }}>
                          <Text style={{ fontWeight: "bold" }}>Issue:</Text> {item.attrs.check_command},{" "}
                          <Text style={{ fontWeight: "bold" }}>IP:</Text>:{item?.joins?.host?.address}
                        </Text>
                      </Body>
                    </ListItem>
                  );
                })}
              </List>
            </Content>
          </CardItem>
        )}
        {data?.serviceList?.results?.length > 10 && (
          <CardItem>
            <Text style={{ fontWeight: "bold" }} onPress={() => navigation.navigate(OverviewStackScreens.ServiceList)}>
              Show more ...
            </Text>
          </CardItem>
        )}
      </Card>
    </RefreshControllerComponent>
  );
};

export default OverviewComponent;

const HeaderWrapper = styled.View`
  width: 80%;
  display: flex;
  justify-content: flex-start;
  flex-direction: row;
`;

const Badge = styled.View`
  background-color: #ffae32;
  text-align: center;
  min-width: 24px;
  max-width: 80px;
  min-height: 24px;
  max-height: 24px;
  border-radius: 24px;
  margin-right: 16px;
  padding: 1px 6px;
`;
const BadgeText = styled.Text`
  font-size: 14px;
  color: #fff;
  text-align: center;
`;
