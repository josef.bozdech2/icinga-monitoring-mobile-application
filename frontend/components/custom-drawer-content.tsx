import { DrawerContentScrollView, DrawerItemList } from "@react-navigation/drawer";
import { Text, View } from "native-base";
import React from "react";
import { Image } from "react-native";

/**
 *
 * @param props props to display from navigator
 * @returns navigator with custom image
 */
export function CustomDrawerContent(props: any) {
  return (
    <DrawerContentScrollView {...props}>
      <View
        style={{
          marginTop: -16,
          paddingTop: 24,
          paddingBottom: 24,
          height: 130,
          backgroundColor: "#01AFEC",
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
        }}>
        <Image
          style={{
            width: 100,
            height: 100,
            resizeMode: "contain",
          }}
          source={require("../assets/adaptive-icon.png")}
        />
        <Text style={{ fontSize: 36, color: "#fff" }}>Kivici</Text>
      </View>
      <DrawerItemList {...props} />
    </DrawerContentScrollView>
  );
}
