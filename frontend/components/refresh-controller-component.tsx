import React from "react";
import { RefreshControl, SafeAreaView, ScrollView, StyleSheet } from "react-native";
interface RefreshControllerComponentProps {
  children: React.ReactNode;
  onRefresh: () => void;
  offset?: number;
}
const RefreshControllerComponent = (props: RefreshControllerComponentProps) => {
  const [refreshing, setRefreshing] = React.useState(false);

  const onRefresh = React.useCallback(async () => {
    setRefreshing(true);
    await props.onRefresh();
    setRefreshing(false);
  }, []);

  return (
    <SafeAreaView>
      <ScrollView refreshControl={<RefreshControl refreshing={refreshing} onRefresh={onRefresh} progressViewOffset={props?.offset} />}>
        {props.children}
      </ScrollView>
    </SafeAreaView>
  );
};

export default RefreshControllerComponent;
