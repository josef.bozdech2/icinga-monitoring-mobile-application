import { Input, Item, Label } from "native-base";
import React from "react";
import { StyleSheet } from "react-native";

type AppInputProps = {
  label: string;
  children: React.ReactNode;
};
/**
 * 
 * @param param0 props for AppInputWrapper component
 * @returns AppInputWrapper component
 */
export const AppInputWrapper = ({ label, children }: AppInputProps) => {
  return (
    <Item style={styles.itemContainer}>
      <Label>{label}</Label>
      {children}
    </Item>
  );
};

const styles = StyleSheet.create({
  itemContainer: {
    paddingTop: 4,
    paddingBottom: 4,
    display: "flex",
    justifyContent: "space-between",
    flexDirection: "row",
  },
});
