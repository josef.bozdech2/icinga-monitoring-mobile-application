import React, { useContext } from "react";
import { Body, Text, ListItem, Left, Right, Icon, Button, Toast } from "native-base";
import { useNavigation } from "@react-navigation/native";
import { Alert, Pressable } from "react-native";
import Axios from "axios";
import AppContext, { AppContextWrapperType } from "../../contexts/app-context";
import { RowObject } from "../../interfaces/icinga.interface";
import { SectionProps } from "../../screens/host-detail";
import { RootStackScreens } from "../../navigators/root-stack/types";
import { useBuildKiviciUrl } from "../../hooks/use-build-kivici-url";
import { ModalFormCallsEnum } from "../../screens/modal-form/calls";
/**
 *
 * @param param0 object of props sent by icinga and on refresh function
 * @returns detail section component
 */
export default function SectionProblemHandling({ data, onRefresh }: SectionProps) {
  const appContext = useContext(AppContext)!;
  const navigation = useNavigation();

  const { buildKiviciUrl } = useBuildKiviciUrl();

  const _attrs = {
    type: data.attrs.type,
    host_name: data.attrs.host_name,
    display_name: data.attrs.display_name,
    state: data.attrs.state,
    acknowledgement: data.attrs.acknowledgement,
  };

  return (
    <>
      <ListItem itemDivider>
        <Text>PROBLEM HANDLING</Text>
      </ListItem>
      {_attrs.state != "0.0" && _attrs.acknowledgement == 0.0 ? (
        <ListItem icon>
          <Pressable
            onPress={() =>
              navigation.navigate(RootStackScreens.ModalForm, {
                attrs: _attrs,
                formItems: acknowledgeFormItems,
                formSubmitCall: ModalFormCallsEnum.acknowledge,
                name: "Acknowledge Problem",
                onRefresh,
              })
            }
            style={{ display: "flex", flexDirection: "row" }}>
            <Left>
              <Button style={{ backgroundColor: "#4caf50" }}>
                <Icon active type="AntDesign" name="form" />
              </Button>
            </Left>
            <Body>
              <Text>Acknowledge</Text>
            </Body>
            <Right>
              <Text>On</Text>
              <Icon active name="arrow-forward" />
            </Right>
          </Pressable>
        </ListItem>
      ) : null}
      {_attrs.state !== "0.0" && _attrs.acknowledgement !== 0.0 ? (
        <ListItem
          icon
          onPress={() => {
            removeAcknowledgement(data, onRefresh, appContext, buildKiviciUrl);
          }}>
          <Left>
            <Button style={{ backgroundColor: "#FF9501" }}>
              <Icon active type="AntDesign" name="close" />
            </Button>
          </Left>
          <Body>
            <Text>Remove Acknowledgement</Text>
          </Body>
          <Right>
            <Icon active name="arrow-forward" />
          </Right>
        </ListItem>
      ) : null}
      <ListItem icon>
        <Pressable
          onPress={() =>
            navigation.navigate("ModalForm", {
              attrs: _attrs,
              formItems: commentsFormItems,
              formSubmitCall: ModalFormCallsEnum.addComment,
              name: "Add Comment",
              onRefresh,
            })
          }
          style={{ display: "flex", flexDirection: "row" }}>
          <Left>
            <Button style={{ backgroundColor: "#007AFF" }}>
              <Icon active type="AntDesign" name="message1" />
            </Button>
          </Left>
          <Body>
            <Text>Comments</Text>
          </Body>
          <Right>
            <Text>Add</Text>
            <Icon active name="arrow-forward" />
          </Right>
        </Pressable>
      </ListItem>
      <ListItem icon>
        <Pressable
          onPress={() =>
            navigation.navigate("ModalForm", {
              attrs: _attrs,
              formItems: _attrs.type === "Host" ? downtimeHostFormItems : downtimeServiceFormItems,
              formSubmitCall: ModalFormCallsEnum.scheduleDowntime,
              name: "Schedule Downtime",
              onRefresh,
            })
          }
          style={{ display: "flex", flexDirection: "row" }}>
          <Left>
            <Button style={{ backgroundColor: "#FF9501" }}>
              <Icon active type="AntDesign" name="tool" />
            </Button>
          </Left>
          <Body>
            <Text>Downtimes</Text>
          </Body>
          <Right>
            <Text>Schedule</Text>
            <Icon active name="arrow-forward" />
          </Right>
        </Pressable>
      </ListItem>
      <ListItem
        icon
        onPress={() => {
          handleCheck(data, appContext, buildKiviciUrl);
        }}>
        <Left>
          <Button style={{ backgroundColor: "#FF9501" }}>
            <Icon active type="AntDesign" name="reload1" />
          </Button>
        </Left>
        <Body>
          <Text>Check now</Text>
        </Body>
        <Right>
          <Icon active name="arrow-forward" />
        </Right>
      </ListItem>
    </>
  );
}
/**
 *
 * @param data list of objects
 * @param param1 object containing expoPushToken
 */
const handleCheck = (data: RowObject, { expoPushToken }: AppContextWrapperType, buildKiviciUrl: (path: string) => string) => {
  const _url = "/icingaProxy/actions/rescheduleCheck";
  let _nameFilter = null;
  if (data.attrs.type === "Service") {
    _nameFilter = `match(\"${data.attrs.host_name}*\", host.name)&&match(\"${data.attrs.display_name}*\", service.name)`;
  } else {
    _nameFilter = `match(\"${data.attrs.display_name}*\", host.name)`;
  }
  Toast.show({
    text: "Rescheduling check now!",
    buttonText: "Okay",
    position: "bottom",
    type: "success",
    style: { bottom: "70%" },
  });
  Axios.post(buildKiviciUrl(_url), { expoPushToken, params: { filter: _nameFilter, type: data.attrs.type, force: true } });
};
/**
 *
 * @param data list of icinga objects
 * @param onRefresh refresh function
 * @param param2 object containing expoPushToken param
 */
const removeAcknowledgement = async (
  data: RowObject,
  onRefresh: () => void,
  { expoPushToken }: AppContextWrapperType,
  buildKiviciUrl: (path: string) => string
) => {
  const _url = "/icingaProxy/actions/removeAck";
  let _nameFilter = null;
  if (data.attrs.type === "Service") {
    _nameFilter = `match(\"${data.attrs.host_name}*\", host.name)&&match(\"${data.attrs.display_name}*\", service.name)`;
  } else {
    _nameFilter = `match(\"${data.attrs.display_name}*\", host.name)`;
  }
  Toast.show({
    text: "Removing acknowledgement now!",
    buttonText: "Okay",
    type: "success",
    position: "bottom",
    style: { bottom: "70%" },
  });
  await Axios.post(buildKiviciUrl(_url), { expoPushToken, params: { filter: _nameFilter, type: data.attrs.type } });
  onRefresh();
};
const commentsFormItems = [{ key: "comment", label: "Comment", type: "TEXTAREA", initialValidation: true }];
const acknowledgeFormItems = [
  { key: "comment", label: "Comment*", type: "TEXTAREA", initialValidation: true },
  { key: "persistent", label: "Persistent Comment", type: "SWITCH" },
  { key: "use_expiry", label: "Use Expire Time ", type: "SWITCH" },
  // { key: "expiry_label", label: "If expiry switched to true, fill End Time", type: "LABEL" },
  // { key: "expiry", label: "End Time", type: "DATE" },
  {
    key: "expiry_date",
    label: "Date end",
    type: "DATE_PICKER",
  },
  {
    key: "expiry_time",
    label: "Time end",
    type: "TIME_PICKER",
  },
  { key: "sticky", label: "Sticky Acknowledgement", type: "SWITCH" },
  { key: "notify", label: "Send Notification", type: "SWITCH" },
];
const downtimeHostFormItems = [
  { key: "comment", label: "Comment*", type: "TEXTAREA", initialValidation: true },
  {
    key: "start_date",
    label: "Date start*",
    type: "DATE_PICKER",
    initialValidation: true,
  },
  {
    key: "start_time",
    label: "Time start*",
    type: "TIME_PICKER",
    initialValidation: true,
  },
  {
    key: "end_date",
    label: "Date end*",
    type: "DATE_PICKER",
    initialValidation: true,
  },
  {
    key: "end_time",
    label: "Time end*",
    type: "TIME_PICKER",
    initialValidation: true,
  },
  {
    key: "fixed",
    label: "Type",
    type: "SELECT",
    items: [{ label: "Fixed" }, { value: "flexible", label: "Flexible" }],
  },
  { key: "fixed_label", label: "If type set to Flexible, fill duration", type: "LABEL" },
  {
    key: "duration_hours",
    label: "Hours",
    type: "NUMBER_INPUT",
  },
  {
    key: "duration_minutes",
    label: "Minutes",
    type: "NUMBER_INPUT",
  },
  // { key: "label", label: "Time format: [h:m:s D.M.YYYY]", type: "LABEL" },
  // { key: "start_time", label: "Start Time*", type: "DATE" },
  // { key: "end_time", label: "End Time*", type: "DATE" },
  { key: "all_services", label: "All Services", type: "SWITCH" },
  {
    key: "child_options",
    label: "Type",
    type: "SELECT",
    items: [
      { label: "Do nothing with child hosts" },
      { value: 1, label: "Schedule triggered downtime for all child hosts" },
      { value: 2, label: "Schedule non-triggered downtime for all child hosts" },
    ],
  },
];
const downtimeServiceFormItems = [
  { key: "comment", label: "Comment*", type: "TEXTAREA", initialValidation: true },
  {
    key: "start_date",
    label: "Date start*",
    type: "DATE_PICKER",
    initialValidation: true,
  },
  {
    key: "start_time",
    label: "Time start*",
    type: "TIME_PICKER",
    initialValidation: true,
  },
  {
    key: "end_date",
    label: "Date end*",
    type: "DATE_PICKER",
    initialValidation: true,
  },
  {
    key: "end_time",
    label: "Time end*",
    type: "TIME_PICKER",
    initialValidation: true,
  },
  { key: "fixed_label", label: "If type set to Flexible, fill duration", type: "LABEL" },
  {
    key: "fixed",
    label: "Type",
    type: "SELECT",
    items: [{ label: "Fixed" }, { value: "flexible", label: "Flexible" }],
  },
  {
    key: "duration_hours",
    label: "Hours",
    type: "NUMBER_INPUT",
  },
  {
    key: "duration_minutes",
    label: "Minutes",
    type: "NUMBER_INPUT",
  },
  // { key: "label", label: "Time format: [h:m:s D.M.YYYY]", type: "LABEL" },
  // { key: "start_time", label: "Start Time*", type: "TEXT" },
  // { key: "end_time", label: "End Time*", type: "TEXT" },
];
