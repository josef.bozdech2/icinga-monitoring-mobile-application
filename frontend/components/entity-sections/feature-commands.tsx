import React from "react";
import { Body, Text, ListItem, Left, Right, Icon, Button, Switch } from "native-base";
import { SectionProps } from "../../screens/host-detail";
/**
 * 
 * @param param0 object of props sent by icinga
 * @returns detail section component
 */
export default function SectionFeatureCommands({ data }: SectionProps) {
  const attrs = data?.attrs;

  return (
    <>
      <ListItem itemDivider>
        <Text>FEATURE COMMANDS</Text>
      </ListItem>
      <ListItem icon>
        <Left>
          <Button style={{ backgroundColor: "#FF9501" }}>
            <Icon active type="AntDesign" name="clockcircle" />
          </Button>
        </Left>
        <Body>
          <Text>Active Checks</Text>
        </Body>
        <Right>
          <Switch value={attrs?.enable_active_checks} />
        </Right>
      </ListItem>
      <ListItem icon>
        <Left>
          <Button style={{ backgroundColor: "#4caf50" }}>
            <Icon active type="AntDesign" name="clockcircle" />
          </Button>
        </Left>
        <Body>
          <Text>Passive Checks</Text>
        </Body>
        <Right>
          <Switch value={attrs?.enable_passive_checks} />
        </Right>
      </ListItem>
      <ListItem icon>
        <Left>
          <Button style={{ backgroundColor: "#007AFF" }}>
            <Icon active type="AntDesign" name="bells" />
          </Button>
        </Left>
        <Body>
          <Text>Notifications</Text>
        </Body>
        <Right>
          <Switch value={attrs?.enable_notifications} />
        </Right>
      </ListItem>
      <ListItem icon>
        <Left>
          <Button style={{ backgroundColor: "#FF9501" }}>
            <Icon active type="AntDesign" name="rocket1" />
          </Button>
        </Left>
        <Body>
          <Text>Event Handler</Text>
        </Body>
        <Right>
          <Switch value={attrs?.enable_event_handler} />
        </Right>
      </ListItem>
      <ListItem icon>
        <Left>
          <Button style={{ backgroundColor: "#007AFF" }}>
            <Icon active name="airplane" />
          </Button>
        </Left>
        <Body>
          <Text>Flap Detection</Text>
        </Body>
        <Right>
          <Switch value={attrs?.enable_flapping} />
        </Right>
      </ListItem>
    </>
  );
}
