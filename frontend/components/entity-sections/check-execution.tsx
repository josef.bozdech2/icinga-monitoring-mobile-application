import React, { useContext } from "react";
import { Body, Text, ListItem, Right, Icon, Toast } from "native-base";
import moment from "moment";
import Axios from "axios";
import AppContext, { AppContextWrapperType } from "../../contexts/app-context";
import { SectionProps } from "../../screens/host-detail";
import { LastCheckResultObject, RowObject } from "../../interfaces/icinga.interface";
import { useBuildKiviciUrl } from "../../hooks/use-build-kivici-url";
/**
 *
 * @param param0 object of props sent by icinga
 * @returns detail section component
 */
export default function SectionCheckExecution({ data }: SectionProps) {
  const appContext = useContext(AppContext)!;

  const { buildKiviciUrl } = useBuildKiviciUrl();

  const attrs = data?.attrs;
  const checkResultData: string | LastCheckResultObject = data?.attrs?.last_check_result || "";
  let executionStart: moment.Moment | 0 = 0;
  let executionEnd: moment.Moment | 0 = 0;
  if (data?.attrs?.last_check_result?.execution_start) executionStart = moment.unix(data?.attrs?.last_check_result?.execution_start);
  if (data?.attrs?.last_check_result?.execution_end) executionEnd = moment.unix(data?.attrs?.last_check_result?.execution_end);

  return (
    <>
      <ListItem itemDivider>
        <Text>CHECK EXECUTION</Text>
      </ListItem>

      <ListItem icon>
        <Body>
          <Text>Command</Text>
        </Body>
        <Right>
          <Text>{data?.attrs?.check_command}</Text>
        </Right>
      </ListItem>

      <ListItem icon>
        <Body>
          <Text>Check Source</Text>
        </Body>
        <Right>
          <Text>{data?.attrs?.last_check_result?.check_source || ""}</Text>
        </Right>
      </ListItem>

      <ListItem icon>
        <Body>
          <Text>Last Check</Text>
        </Body>
        <Right>
          <Text>{moment.unix(attrs?.last_check).fromNow()}</Text>
          <Icon
            active
            name="refresh"
            type="FontAwesome"
            style={{ color: "#007AFF" }}
            onPress={() => {
              handleCheck(data, appContext, buildKiviciUrl);
            }}
          />
        </Right>
      </ListItem>

      <ListItem icon>
        <Body>
          <Text>Next Check</Text>
        </Body>
        <Right>
          <Text>{moment.unix(attrs?.next_check).fromNow()}</Text>
        </Right>
      </ListItem>

      <ListItem icon>
        <Body>
          <Text>Check Attempts</Text>
        </Body>
        <Right>
          <Text>{`${attrs?.check_attempt}/${attrs?.max_check_attempts}`}</Text>
        </Right>
      </ListItem>

      <ListItem icon>
        <Body>
          <Text>Check Execution Time</Text>
        </Body>
        <Right>
          <Text>{`${moment(executionEnd).diff(executionStart, "millisecond")} ms`}</Text>
        </Right>
      </ListItem>
    </>
  );
}
/**
 *
 * @param data data from previous icinga request
 * @param param1 unique user token
 */
const handleCheck = (data: RowObject, { expoPushToken }: AppContextWrapperType, buildKiviciUrl: (path: string) => string) => {
  const _url = "/icingaProxy/actions/rescheduleCheck";
  let _nameFilter = null;
  if (data.attrs.type === "Service") {
    _nameFilter = `match(\"${data.attrs.host_name}*\", host.name)&&match(\"${data.attrs.display_name}*\", service.name)`;
  } else {
    _nameFilter = `match(\"${data.attrs.display_name}*\", host.name)`;
  }
  Toast.show({
    text: "Rescheduling check now!",
    buttonText: "Okay",
    position: "bottom",
    type: "success",
    style: { bottom: "70%" },
  });
  Axios.post(buildKiviciUrl(_url), { expoPushToken, params: { filter: _nameFilter, type: data.attrs.type, force: true } });
};
