import React from "react";
import { Body, Text, ListItem, Left, Right, Icon, Button } from "native-base";
import { HOST_COLOR_ENUM, SERVICE_COLOR_ENUM} from "../../enums/state-color-enum";
import { SectionProps } from "../../screens/host-detail";
/**
 * 
 * @param param0 object of props sent by icinga
 * @returns detail section component
 */
export default function SectionPluginOutput({ data }: SectionProps) {
  return (
    <>
      <ListItem itemDivider>
        <Text>PLUGIN OUTPUT</Text>
      </ListItem>
      <ListItem>
        <Text>{data?.attrs?.last_check_result?.output || "UNKNOWN STATE"}</Text>
      </ListItem>
    </>
  );
}

