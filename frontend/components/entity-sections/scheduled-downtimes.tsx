import React, { useContext } from "react";
import { Body, Text, ListItem, Left, Right, Icon, Button, Spinner } from "native-base";
import moment from "moment";
import Axios from "axios";
import { Alert, Pressable } from "react-native";
import useData from "../../hooks/use-data";
import AppContext, { AppContextWrapperType } from "../../contexts/app-context";
import ConfigContext from "../../contexts/config-context";
import { AttrsObject, DowntimeObject, RowObject } from "../../interfaces/icinga.interface";
import { SectionProps } from "../../screens/host-detail";
import { useBuildKiviciUrl } from "../../hooks/use-build-kivici-url";
/**
 *
 * @param props object of props sent by icinga and on refresh function
 * @returns list of scheduled downtimes
 */
export default function SectionScheduledDowntimes(props: SectionProps) {
  const attrs: AttrsObject = props?.data?.attrs;

  const appContext = useContext(AppContext)!;
  const { config } = useContext(ConfigContext)!;

  const { buildKiviciUrl } = useBuildKiviciUrl();

  const { data, isLoading } = useData({
    config,
    url: `/icingaProxy/objects/downtimes`,
    dtoIn: getParams(attrs, appContext?.expoPushToken as string),
    refreshInterval: 30000,
  });
  if (isLoading) return <Spinner />;

  return (
    <>
      <ListItem itemDivider>
        <Text>SCHEDULED DOWNTIMES</Text>
      </ListItem>
      {isLoading && <Spinner />}
      {!isLoading &&
        data?.results?.map((item: DowntimeObject) => {
          return (
            <ListItem key={item.attrs.entry_time}>
              <Left>
                <Text>{item.attrs.comment}</Text>
              </Left>
              <Body>
                <Text note>{item.attrs.name}</Text>
                <Text note>{moment.unix(item.attrs.entry_time).fromNow()}</Text>
              </Body>
              <Right>
                <Pressable
                  style={{ width: 40, height: 40, padding: 10 }}
                  onPress={() =>
                    Alert.alert(
                      "Do you really want to remove this scheduled downtime",
                      item.attrs.comment,
                      [
                        {
                          text: "Remove",
                          onPress: () => removeDowntime(item.attrs.__name, props.onRefresh, appContext, buildKiviciUrl),
                          style: "destructive",
                        },
                        {
                          text: "Cancel",
                          style: "cancel",
                        },
                      ],
                      {
                        cancelable: true,
                      }
                    )
                  }>
                  <Icon style={{ fontSize: 20, color: "red" }} name="remove-circle" type="MaterialIcons" />
                </Pressable>
              </Right>
            </ListItem>
          );
        })}
    </>
  );
}
/**
 *
 * @param attrs shown object info
 * @param expoPushToken device token
 * @returns params ready to send via axios
 */
function getParams(attrs: AttrsObject, expoPushToken: string) {
  let params = {};
  if (attrs.type === "Service") {
    params = {
      expoPushToken,
      type: "Service",
      filter: `match(\"${attrs.host_name}\", host.name)&&match(\"${attrs.display_name}\", service.name)`,
    };
  } else {
    params = {
      expoPushToken,
      type: "Host",
      filter: `match(\"${attrs.display_name}\", host.name)`,
    };
  }
  return params;
}
/**
 *
 * @param __name downtime object name
 * @param onRefresh onRefresh function
 * @param appContext appContext context
 */
async function removeDowntime(
  __name: string,
  onRefresh: () => void,
  appContext: AppContextWrapperType,
  buildKiviciUrl: (path: string) => string
) {
  try {
    await Axios.post(buildKiviciUrl("/icingaProxy/actions/removeDowntime"), { expoPushToken: appContext.expoPushToken, downtime: __name });
    onRefresh();
  } catch (error) {
    Alert.alert("Comment could not be deleted");
  }
}
