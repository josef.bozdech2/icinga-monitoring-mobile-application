import React from "react";
import { Body, Text, ListItem, Left, Right, Icon, Button } from "native-base";
import { SectionProps } from "../../screens/host-detail";
/**
 * 
 * @param param0 object of props sent by icinga
 * @returns detail section component
 */
export default function SectionPerformanceData({ data }: SectionProps) {
  const performanceData = data?.attrs?.last_check_result?.performance_data;
  if (performanceData?.length === 0) {
    return (
      <>
        <ListItem itemDivider>
          <Text>PERFORMANCE DATA</Text>
        </ListItem>
        <ListItem key="data">
          <Left>
            <Text>No performace data received</Text>
          </Left>
        </ListItem>
      </>
    );
  }

  return (
    <>
      <ListItem itemDivider>
        <Text>PERFORMANCE DATA</Text>
      </ListItem>
      {data?.attrs?.check_command !== 'icinga' && performanceData?.length &&
        performanceData.map((item) => {
          const [label, value, warning, critical] = item.split(";");
          return (
            <ListItem key={label}>
              <Left>
                <Text>{label}</Text>
              </Left>
            </ListItem>
          );
        })}
    </>
  );
}
