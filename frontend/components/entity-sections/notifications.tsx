import React from "react";
import { Body, Text, ListItem, Left, Right, Icon, Button } from "native-base";
import { Pressable } from "react-native";
import { useNavigation } from "@react-navigation/native";
import { RowObject } from "../../interfaces/icinga.interface";
import { SectionProps } from "../../screens/host-detail";
import { ModalFormCallsEnum } from "../../screens/modal-form/calls";
import { RootStackScreens } from "../../navigators/root-stack/types";
/**
 *
 * @param param0 object of props sent by icinga
 * @returns detail section component
 */
export default function SectionNotifications({ data }: SectionProps) {
  const _attrs = { type: data.attrs.type, host_name: data.attrs.host_name, display_name: data.attrs.display_name };
  const navigation = useNavigation();
  return (
    <>
      <ListItem itemDivider>
        <Text>NOTIFICATIONS</Text>
      </ListItem>
      <ListItem icon>
        <Pressable
          onPress={() =>
            navigation.navigate(RootStackScreens.ModalForm, {
              attrs: _attrs,
              formItems: notificationFormItems,
              formSubmitCall: ModalFormCallsEnum.sendCustomNotification,
              name: "Send Custom Notification",
            })
          }
          style={{ display: "flex", flexDirection: "row" }}>
          <Left>
            <Button style={{ backgroundColor: "#007AFF" }}>
              <Icon active type="AntDesign" name="bells" />
            </Button>
          </Left>
          <Body>
            <Text>Notification</Text>
          </Body>
          <Right>
            <Text>Send</Text>
            <Icon active name="arrow-forward" />
          </Right>
        </Pressable>
      </ListItem>
    </>
  );
}
const notificationFormItems = [{ key: "comment", label: "Comment", type: "TEXTAREA", initialValidation: true }];
