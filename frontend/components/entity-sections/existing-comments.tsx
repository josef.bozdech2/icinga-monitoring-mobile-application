import Axios from "axios";
import moment from "moment";
import { Body, Icon, Left, ListItem, Right, Spinner, Text } from "native-base";
import React, { useContext } from "react";
import { Alert, Pressable } from "react-native";
import AppContext from "../../contexts/app-context";
import ConfigContext from "../../contexts/config-context";
import { useBuildKiviciUrl } from "../../hooks/use-build-kivici-url";
import useData from "../../hooks/use-data";
import { AttrsObject, CommentObject } from "../../interfaces/icinga.interface";
import { SectionProps } from "../../screens/host-detail";
/**
 *
 * @param props object of props sent by icinga
 * @returns detail section component
 */
export default function SectionExistingComments(props: SectionProps) {
  const appContext = useContext(AppContext)!;
  const { config } = useContext(ConfigContext)!;

  const { buildKiviciUrl } = useBuildKiviciUrl();

  const attrs = props?.data?.attrs;
  const { data, isLoading } = useData({
    config,
    url: `/icingaProxy/objects/comments`,
    dtoIn: getParams(attrs, appContext?.expoPushToken as string),
    refreshInterval: 30000,
  });
  if (isLoading) return <Spinner />;

  async function removeComment(__name: string) {
    try {
      await Axios.post(buildKiviciUrl("/icingaProxy/actions/removeComment"), { expoPushToken: appContext.expoPushToken, comment: __name });
      props.onRefresh();
    } catch (error) {
      console.log(error);
      Alert.alert("Comment could not be deleted");
    }
  }

  return (
    <>
      <ListItem itemDivider>
        <Text>EXISTING COMMENTS</Text>
      </ListItem>
      {isLoading && <Spinner />}
      {!isLoading &&
        data?.results?.map((item: CommentObject) => {
          return (
            <ListItem key={item.attrs.entry_time}>
              <Left>
                <Text>{item.attrs.text}</Text>
              </Left>
              <Body>
                <Text note>{item.attrs.author}</Text>
                <Text note>{moment.unix(item.attrs.entry_time).fromNow()}</Text>
              </Body>
              <Right>
                <Pressable
                  style={{ width: 40, height: 40, padding: 10 }}
                  onPress={() =>
                    Alert.alert(
                      "Do you really want to remove this comment",
                      item.attrs.text,
                      [
                        {
                          text: "Remove",
                          onPress: () => removeComment(item.attrs.__name),
                          style: "destructive",
                        },
                        {
                          text: "Cancel",
                          style: "cancel",
                        },
                      ],
                      {
                        cancelable: true,
                      }
                    )
                  }>
                  <Icon style={{ fontSize: 20, color: "red" }} name="remove-circle" type="MaterialIcons" />
                </Pressable>
              </Right>
            </ListItem>
          );
        })}
    </>
  );
}
/**
 *
 * @param attrs atributes sent by icinga
 * @param expoPushToken unique user token
 * @returns params to be sent via API request
 */
function getParams(attrs: AttrsObject, expoPushToken: string) {
  let params = {};
  if (attrs.type === "Service") {
    params = {
      expoPushToken,
      type: "Service",
      filter: `match(\"${attrs.host_name}\", host.name)&&match(\"${attrs.display_name}\", service.name)`,
    };
  } else {
    params = {
      expoPushToken,
      type: "Host",
      filter: `match(\"${attrs.display_name}\", host.name)`,
    };
  }
  return params;
}
