import React from "react";
import { Text, ListItem } from "native-base";
import { useNavigation } from "@react-navigation/native";
import { RowObject } from "../../interfaces/icinga.interface";
import { SectionProps } from "../../screens/host-detail";
import { HostStackScreens } from "../../navigators/host-stack/types";
/**
 * 
 * @param param0 object of props sent by icinga
 * @returns detail section component
 */
export default function SectionMoveToHost({ data }: SectionProps) {
  const navigation = useNavigation();
  return (
    <>
      <ListItem itemDivider onPress={() => navigation.navigate(HostStackScreens.HostDetail, { rowId: data.attrs.host_name })}>
        <Text>MOVE TO HOST INFO</Text>
      </ListItem>
    </>
  );
}
