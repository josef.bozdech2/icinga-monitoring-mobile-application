import React from "react";
import { Text, ListItem } from "native-base";
import { useNavigation } from "@react-navigation/native";
import { RowObject } from "../../interfaces/icinga.interface";
import { SectionProps } from "../../screens/host-detail";
import { ServiceStackScreens } from "../../navigators/service-stack/types";
/**
 * 
 * @param param0 object of props sent by icinga
 * @returns detail section component
 */
export default function SectionMoveToServiceList({ data }: SectionProps) {
  const navigation = useNavigation();
  return (
    <>
      <ListItem itemDivider onPress={() => navigation.navigate(ServiceStackScreens.ServiceList, { filterValue: data.attrs.name, initial: false })}>
        <Text>MOVE TO SERVICE LIST</Text>
      </ListItem>
    </>
  );
}
