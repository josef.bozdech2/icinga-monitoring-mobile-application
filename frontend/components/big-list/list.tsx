import { View } from "native-base";
import React from "react";
import BigList from "react-native-big-list";
import { RowObject } from "../../interfaces/icinga.interface";
import BigListItem from "./item";

interface Props {
  data: Array<RowObject>;
  onRefresh?: () => {};
  itemHeight?: number;
  onRowClick: (row: RowObject) => void;
}
/**
 *
 * @param param0 object of required params
 * @returns big list component
 */
export const List: React.FC<Props> = ({ data, onRefresh, onRowClick, itemHeight = 80 }) => {
  function _renderItem({ item, index }: { item: RowObject, index: number }) {
    return <BigListItem row={item} index={index} itemHeight={itemHeight} onRowClick={onRowClick} />;
  }

  return (
    <View style={{ flex: 1, height: "100%", width: "100%" }}>
      <BigList data={data} renderItem={_renderItem} itemHeight={itemHeight} keyExtractor={(item) => item.name} onRefresh={onRefresh} />
    </View>
  );
};

export default React.memo(List);
