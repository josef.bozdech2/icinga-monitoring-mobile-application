import moment from "moment";
import { Icon, Text } from "native-base";
import React from "react";
import { Pressable, StyleSheet, View } from "react-native";
import { RowObject } from "../../interfaces/icinga.interface";

interface Props {
  row: RowObject;
  index: number;
  itemHeight?: number;
  onRowClick(row: RowObject): void;
}
/**
 *
 * @param param0 object of required params
 * @returns big list item component
 */
const BigListItem: React.FC<Props> = ({ row, index, onRowClick, itemHeight = 80 }) => {
  return (
    <Pressable key={row.name} onPress={() => onRowClick(row)}>
      <View
        style={{
          ...styles.containerStyle,
          backgroundColor: index % 2 ? "#EBEBEB" : "#fff",
          height: itemHeight,
        }}>
        <View style={styles.leftIconStyle}>
          {(row.attrs.state == "2" && row.type === "Service") || (row.attrs.state == "1" && row.type === "Host") ? (
            <Icon style={{ fontSize: 30, color: "red" }} name="remove-circle" type="MaterialIcons" />
          ) : (
            <></>
          )}
          {row.attrs.state == "0" ? <Icon style={{ fontSize: 30, color: "limegreen" }} name="check-circle" type="MaterialIcons" /> : <></>}
          {row.attrs.state == "1" && row.type === "Service" ? (
            <Icon style={{ fntSize: 30, color: "orange" }} name="exclamationcircle" type="AntDesign" />
          ) : (
            <></>
          )}
          {row.attrs.state == "3" ? <Icon style={{ fontSize: 30, color: "purple" }} name="questioncircle" type="AntDesign" /> : <></>}
        </View>
        <View style={styles.centerBodyStyle}>
          <Text style={{ fontSize: 14 }}>{row.name}</Text>
          <Text note>{row.type}</Text>
        </View>
        <View style={styles.rightTimeStyle}>
          <Text numberOfLines={1} style={{ fontSize: 12, color: "#77777F" }}>
            {moment.unix(row.attrs.last_check).format("LT")}
          </Text>
        </View>
      </View>
    </Pressable>
  );
};

export default React.memo(
  BigListItem,
  (prevProps, nextProps) => prevProps.row.name === nextProps.row.name && prevProps.row.attrs.last_check === nextProps.row.attrs.last_check
);

const styles = StyleSheet.create({
  containerStyle: {
    display: "flex",
    justifyContent: "center",
    flexDirection: "row",
    paddingTop: 16,
    paddingRight: 16,
    paddingBottom: 16,
    width: "100%",
  },
  leftIconStyle: {
    display: "flex",
    width: 48,
    height: "100%",
    paddingLeft: 8,
    alignContent: "center",
    justifyContent: "center",
  },
  centerBodyStyle: {
    display: "flex",
    alignContent: "center",
    justifyContent: "center",
    flex: 2,
  },
  rightTimeStyle: {
    alignContent: "center",
    justifyContent: "center",
    flexDirection: "row",
    paddingTop: 16,
    flex: 1,
  },
});
