import { Form, Header, Icon, Input, Item, Picker } from "native-base";
import React, { useState } from "react";
import { Transition, Transitioning, TransitioningView } from "react-native-reanimated";

export interface BigListFilterHeaderProps {
  objectType: string;
  handleFilterChange(value: string | null): void;
  filterValue: string | null;
  picker: string;
  handlePickerChange: React.Dispatch<React.SetStateAction<string>>;
}
/**
 *
 * @param props object of required params
 * @returns big list header component
 */
export const BigListFilterHeader: React.FC<BigListFilterHeaderProps> = (props: BigListFilterHeaderProps) => {
  const ref = React.useRef<TransitioningView | null>(null);
  const [isTyping, setIsTyping] = useState(false);

  function handleSearch(value: string | null) {
    if (!value) {
      if (isTyping) {
        ref.current?.animateNextTransition();
        setIsTyping(false);
      }
      props.handleFilterChange(null);
    } else {
      if (!isTyping) {
        ref.current?.animateNextTransition();
        setIsTyping(true);
      }
      props.handleFilterChange(value);
    }
  }

  return (
    <Header searchBar rounded>
      <Item>
        <Transitioning.View ref={ref} transition={transition}>
          {isTyping ? <Icon name="ios-trash" style={{ color: "red" }} onPress={() => handleSearch(null)} /> : <Icon name="ios-search" />}
        </Transitioning.View>

        <Input style={{ fontSize: 14 }} value={props?.filterValue || undefined} placeholder="Search" onChange={(e) => handleSearch(e.nativeEvent.text)} />
        <Form>
          {props.objectType === "Service" ? (
            <Picker
              mode="dropdown"
              style={{ width: 140, display: "flex", justifyContent: "flex-end" }}
              selectedValue={props.picker}
              onValueChange={(value: string) => props.handlePickerChange(value)}>
              <Picker.Item label="Problems" value="-1" />
              <Picker.Item label="All" value="0" />
              <Picker.Item label="Ok" value="0.0" />
              <Picker.Item label="Warning" value="1.0" />
              <Picker.Item label="Critical" value="2.0" />
              <Picker.Item label="Unknown" value="3.0" />
            </Picker>
          ) : (
            <Picker
              mode="dropdown"
              style={{ width: 140, display: "flex", justifyContent: "flex-end" }}
              selectedValue={props.picker}
              onValueChange={(value: string) => props.handlePickerChange(value)}>
              <Picker.Item label="Problems" value="-1" />
              <Picker.Item label="All" value="0" />
              <Picker.Item label="Ok" value="0.0" />
              <Picker.Item label="Critical" value="1.0" />
            </Picker>
          )}
        </Form>
      </Item>
    </Header>
  );
};

const transition = (
  <Transition.Together>
    <Transition.Out type="scale" durationMs={100} />
    <Transition.Change interpolation="easeInOut" />
    <Transition.In type="scale" durationMs={100} delayMs={50} />
  </Transition.Together>
);
