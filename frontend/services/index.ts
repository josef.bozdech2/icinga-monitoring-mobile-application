import env from "../env";
/**
 *
 * @param path path to backend api endpoints
 * @returns built URL to backend
 */
export function buildURL(path: string) {
  //TODO: Get backend url from local storage
  return `${env.APPINGA_BACKEND_URL}${path}`;
}

/**
 *
 * @param obj1 first object to compare
 * @param obj2 second object to compare
 * @returns true if equals, false if not
 */
export const shallowCompare = (obj1: any, obj2: any) =>
  Object.keys(obj1).length === Object.keys(obj2).length &&
  Object.keys(obj1).every((key) => obj2?.hasOwnProperty(key) && obj1[key] === obj2[key]);
