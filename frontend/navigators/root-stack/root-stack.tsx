import { createStackNavigator } from "@react-navigation/stack";
import Axios from "axios";
import { reloadAsync } from "expo-updates";
import { Toast } from "native-base";
import React, { useContext, useEffect } from "react";
import ServerContext from "../../contexts/server-context";
import { ModalForm } from "../../screens/modal-form";
import { AuthStackNavigator } from "../auth-stack/auth-stack";
import { NotAuthStackNavigator } from "../not-auth-stack/not-auth-stack";
import { RootStackParams, RootStackScreens } from "./types";

const RootStack = createStackNavigator<RootStackParams>();
/**
 * Main navigator that contains all other navigators
 * @returns RootStackNavigator
 */
export const RootStackNavigator = () => {
  const { serverConfig, setServer } = useContext(ServerContext)!;

  const isAuthenticated = serverConfig.isServerConnectionValid;

  // INFO: Revalidate server connection to prevent stuck in loading state
  useEffect(() => {
    revalidateServerConnection();
  }, []);
  /**
   * Check for server connection
   */
  const revalidateServerConnection = async () => {
    try {
      const port = serverConfig.port ? ":" + serverConfig.port : "";
      await Axios.get(`http://${serverConfig.host}${port}/`, { timeout: 4000 });
    } catch (error) {
      Toast.show({
        text: "Server connection is invalid",
        position: "bottom",
        type: "danger",
      });
      setServer({ ...serverConfig, isServerConnectionValid: false });
    }
  };

  return (
    <RootStack.Navigator mode="modal">
      {isAuthenticated ? (
        <RootStack.Screen name={RootStackScreens.AuthenticatedStack} component={AuthStackNavigator} options={{ headerShown: false }} />
      ) : (
        <RootStack.Screen
          name={RootStackScreens.NotAuthenticatedStack}
          component={NotAuthStackNavigator}
          options={{ headerShown: false }}
        />
      )}
      <RootStack.Screen
        name={RootStackScreens.ModalForm}
        component={ModalForm}
        options={({ route }) => ({ title: route.params.name || "ModalForm" })}
      />
    </RootStack.Navigator>
  );
};
