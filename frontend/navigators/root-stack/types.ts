import { RouteProp } from "@react-navigation/core";
import { StackNavigationProp } from "@react-navigation/stack";
import { AttrsObject } from "../../interfaces/icinga.interface";
import { FormItems } from "../../screens/modal-form";
import { ModalFormCallsEnum } from "../../screens/modal-form/calls";
/**
 * RootStackParams types definition
 */
export type ModalFormTypes = {
  attrs: AttrsObject;
  formItems: FormItems[];
  formSubmitCall: ModalFormCallsEnum;
  name: "string";
  onRefresh?: () => void;
};
export type RootStackParams = {
  [RootStackScreens.AuthenticatedStack]: undefined;
  [RootStackScreens.NotAuthenticatedStack]: undefined;
  [RootStackScreens.ModalForm]: ModalFormTypes;
};
type RootScreenKeys = keyof RootStackParams;

type RootStackRoutesProps<ScreenName extends RootScreenKeys> = RouteProp<RootStackParams, ScreenName>;

export type RootNavigationProps<ScreenName extends RootScreenKeys> = StackNavigationProp<RootStackParams, ScreenName>;
export type RootStackScreenProps<ScreenName extends RootScreenKeys> = {
  route: RootStackRoutesProps<ScreenName>;
  navigation: RootNavigationProps<ScreenName>;
};
export enum RootStackScreens {
  AuthenticatedStack = "AuthenticatedStack",
  NotAuthenticatedStack = "NotAuthenticatedStack",
  ModalForm = "ModalForm",
}
