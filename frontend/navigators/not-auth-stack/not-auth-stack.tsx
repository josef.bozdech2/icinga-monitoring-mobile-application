import { createDrawerNavigator } from "@react-navigation/drawer";
import { Icon } from "native-base";
import React from "react";
import { CustomDrawerContent } from "../../components/custom-drawer-content";
import ServerSelectScreen from "../../screens/server-select-screen";
import { NotAuthStackParams, NotAuthStackScreens } from "./types";

const NotAuthStack = createDrawerNavigator<NotAuthStackParams>();
/**
 * If user hasn’t added backend config
 * @returns NotAuthStackNavigator
 */
export const NotAuthStackNavigator = () => {
  return (
    <NotAuthStack.Navigator
      initialRouteName={NotAuthStackScreens.ServerSelectScreen}
      drawerContent={(props) => <CustomDrawerContent {...props} />}>
      <NotAuthStack.Screen
        name={NotAuthStackScreens.ServerSelectScreen}
        component={ServerSelectScreen}
        options={{
          title: "Backend config",
          drawerIcon: () => <Icon style={{ fontSize: 40 }} active type="AntDesign" name="cloud" />,
        }}
      />
    </NotAuthStack.Navigator>
  );
};
