import { RouteProp } from "@react-navigation/core";
import { StackNavigationProp } from "@react-navigation/stack";
/**
 * NotAuthStackParams types definition
 */
export type NotAuthStackParams = {
  [NotAuthStackScreens.ServerSelectScreen]: undefined;
};
type NotAuthScreenKeys = keyof NotAuthStackParams;

type NotAuthStackRoutesProps<ScreenName extends NotAuthScreenKeys> = RouteProp<NotAuthStackParams, ScreenName>;

export type NotAuthNavigationProps<ScreenName extends NotAuthScreenKeys> = StackNavigationProp<NotAuthStackParams, ScreenName>;
export type NotAuthStackScreenProps<ScreenName extends NotAuthScreenKeys> = {
  route: NotAuthStackRoutesProps<ScreenName>;
  navigation: NotAuthNavigationProps<ScreenName>;
};
export enum NotAuthStackScreens {
  ServerSelectScreen = "ServerSelectScreen",
}
