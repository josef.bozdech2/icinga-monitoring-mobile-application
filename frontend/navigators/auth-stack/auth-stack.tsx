import { createDrawerNavigator, DrawerContentScrollView, DrawerItemList } from "@react-navigation/drawer";
import { Icon, Spinner, Text, View } from "native-base";
import React, { useContext, useEffect } from "react";
import { Image } from "react-native";
import { CustomDrawerContent } from "../../components/custom-drawer-content";
import AppContext from "../../contexts/app-context";
import ConfigContext from "../../contexts/config-context";
import { getIsOSVersionValid } from "../../helpers/getIsOSVersionValid";
import useNotifications from "../../hooks/use-notifications";
import { useTokenRegistration } from "../../hooks/use-token-registration";
import ConfigScreen from "../../screens/config-screen";
import OverviewScreen from "../../screens/overview-screen";
import ServerSelectScreen from "../../screens/server-select-screen";
import { HostStackNavigator } from "../host-stack/host-stack";
import { OverviewStackNavigator } from "../overview-stack/overview-stack";
import { ServiceStackNavigator } from "../service-stack/service-stack";
import { AuthStackParams, AuthStackScreens } from "./types";

const AuthDrawerNavigator = createDrawerNavigator<AuthStackParams>();
/**
 * Stack navigator if user has set backend
 * @returns AuthStackNavigator
 */
export const AuthStackNavigator = () => {
  const { config } = useContext(ConfigContext)!;

  const { setIsValidOS, isSubscribed } = useContext(AppContext)!;
  const { expoPushToken } = useNotifications();
  useTokenRegistration(expoPushToken);

  useEffect(() => {
    setIsValidOS(getIsOSVersionValid());
  }, []);

  if (!isSubscribed) return <Spinner />;

  if (!config) {
    return (
      <AuthDrawerNavigator.Navigator initialRouteName={AuthStackScreens.ConfigScreen}>
        <AuthDrawerNavigator.Screen name={AuthStackScreens.OverviewScreen} component={OverviewScreen} />
        <AuthDrawerNavigator.Screen name={AuthStackScreens.ConfigScreen} component={ConfigScreen} />
        <AuthDrawerNavigator.Screen
        name={AuthStackScreens.ServerSelectScreen}
        component={ServerSelectScreen}
        options={{
          title: "Backend config",
          drawerIcon: () => <Icon style={{ fontSize: 40 }} active type="AntDesign" name="cloud" />,
        }}
      />
      </AuthDrawerNavigator.Navigator>
    );
  }

  return (
    <AuthDrawerNavigator.Navigator
      initialRouteName={AuthStackScreens.OverviewScreen}
      drawerContent={(props) => <CustomDrawerContent {...props} />}>
      <AuthDrawerNavigator.Screen
        name={AuthStackScreens.OverviewScreen}
        component={OverviewStackNavigator}
        options={{
          title: "Overview Screen",
          drawerIcon: () => <Icon style={{ fontSize: 40 }} active type="AntDesign" name="dashboard" />,
        }}
      />
      <AuthDrawerNavigator.Screen
        name={AuthStackScreens.ServiceStack}
        component={ServiceStackNavigator}
        options={{
          title: "Service List",
          drawerIcon: () => <Icon style={{ fontSize: 40 }} active type="MaterialIcons" name="miscellaneous-services" />,
        }}
      />
      <AuthDrawerNavigator.Screen
        name={AuthStackScreens.HostStack}
        component={HostStackNavigator}
        options={{
          title: "Host List",
          drawerIcon: () => <Icon style={{ fontSize: 40 }} active type="FontAwesome5" name="server" />,
        }}
      />
      <AuthDrawerNavigator.Screen
        name={AuthStackScreens.ConfigScreen}
        component={ConfigScreen}
        options={{
          title: "Configuration",
          drawerIcon: () => <Icon style={{ fontSize: 40 }} active type="AntDesign" name="setting" />,
        }}
      />
      <AuthDrawerNavigator.Screen
        name={AuthStackScreens.ServerSelectScreen}
        component={ServerSelectScreen}
        options={{
          title: "Backend config",
          drawerIcon: () => <Icon style={{ fontSize: 40 }} active type="AntDesign" name="cloud" />,
        }}
      />
    </AuthDrawerNavigator.Navigator>
  );
};
