import { RouteProp } from "@react-navigation/core";
import { StackNavigationProp } from "@react-navigation/stack";
/**
 * AuthStackParams types definition
 */
export type AuthStackParams = {
  [AuthStackScreens.OverviewScreen]: undefined;
  [AuthStackScreens.ServiceStack]: undefined;
  [AuthStackScreens.HostStack]: undefined;
  [AuthStackScreens.ConfigScreen]: undefined;
  [AuthStackScreens.ServerSelectScreen]: undefined;
};
type AuthScreenKeys = keyof AuthStackParams;

type AuthStackRoutesProps<ScreenName extends AuthScreenKeys> = RouteProp<AuthStackParams, ScreenName>;

export type AuthNavigationProps<ScreenName extends AuthScreenKeys> = StackNavigationProp<AuthStackParams, ScreenName>;
export type AuthStackScreenProps<ScreenName extends AuthScreenKeys> = {
  route: AuthStackRoutesProps<ScreenName>;
  navigation: AuthNavigationProps<ScreenName>;
};
export enum AuthStackScreens {
  OverviewScreen = "OverviewScreen",
  ServiceStack = "ServiceStack",
  HostStack = "HostStack",
  ConfigScreen = "ConfigScreen",
  ServerSelectScreen = "ServerSelectScreen",
}
