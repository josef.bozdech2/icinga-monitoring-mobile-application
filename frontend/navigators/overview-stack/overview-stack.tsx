import { createStackNavigator } from "@react-navigation/stack";
import React from "react";
import HostDetail from "../../screens/host-detail";
import HostList from "../../screens/host-list";
import OverviewScreen from "../../screens/overview-screen";
import ServiceDetail from "../../screens/service-detail";
import ServiceList from "../../screens/service-list";
import { OverviewStackParams, OverviewStackScreens } from "./types";

const OverviewStack = createStackNavigator<OverviewStackParams>();
/**
 * Navigator for moving by clicking from overview screen
 * @returns OverviewStackNavigator
 */
export const OverviewStackNavigator = () => {
  return (
    <OverviewStack.Navigator screenOptions={{ headerShown: false }}>
      <OverviewStack.Screen name={OverviewStackScreens.OverviewScreen} component={OverviewScreen} />
      <OverviewStack.Screen name={OverviewStackScreens.ServiceDetail} component={ServiceDetail} />
      <OverviewStack.Screen name={OverviewStackScreens.HostList} component={HostList} />
      <OverviewStack.Screen name={OverviewStackScreens.HostDetail} component={HostDetail} />
      <OverviewStack.Screen name={OverviewStackScreens.ServiceList} component={ServiceList} />
    </OverviewStack.Navigator>
  );
};
