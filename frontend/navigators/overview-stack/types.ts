import { RouteProp } from "@react-navigation/core";
import { StackNavigationProp } from "@react-navigation/stack";
/**
 * OverviewStackParams types definition
 */
export type OverviewStackParams = {
  [OverviewStackScreens.OverviewScreen]: undefined;
  [OverviewStackScreens.ServiceDetail]: undefined;
  [OverviewStackScreens.HostDetail]: undefined;
  [OverviewStackScreens.HostList]: undefined;
  [OverviewStackScreens.ServiceList]: undefined;
};
type OverviewScreenKeys = keyof OverviewStackParams;

type OverviewStackRoutesProps<ScreenName extends OverviewScreenKeys> = RouteProp<OverviewStackParams, ScreenName>;

export type OverviewNavigationProps<ScreenName extends OverviewScreenKeys> = StackNavigationProp<OverviewStackParams, ScreenName>;
export type OverviewStackScreenProps<ScreenName extends OverviewScreenKeys> = {
  route: OverviewStackRoutesProps<ScreenName>;
  navigation: OverviewNavigationProps<ScreenName>;
};
export enum OverviewStackScreens {
  OverviewScreen = "OverviewScreen",
  ServiceDetail = "ServiceDetail",
  HostDetail = "HostDetail",
  ServiceList = "ServiceList",
  HostList = "HostList",
}
