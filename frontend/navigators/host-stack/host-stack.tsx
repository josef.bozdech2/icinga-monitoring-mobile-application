import { createStackNavigator } from "@react-navigation/stack";
import React from "react";
import HostDetail from "../../screens/host-detail";
import HostList from "../../screens/host-list";
import ServiceList from "../../screens/service-list";
import { HostStackParams, HostStackScreens } from "./types";

const HostStack = createStackNavigator<HostStackParams>();
/**
 * HostStack navigator
 * @returns HostStackNavigator
 */
export const HostStackNavigator = () => {
  return (
    <HostStack.Navigator screenOptions={{ headerShown: false }}>
      <HostStack.Screen name={HostStackScreens.HostList} component={HostList} />
      <HostStack.Screen name={HostStackScreens.HostDetail} component={HostDetail} />
      <HostStack.Screen name={HostStackScreens.ServiceList} component={ServiceList} />
    </HostStack.Navigator>
  );
};
