import { RouteProp } from "@react-navigation/core";
import { StackNavigationProp } from "@react-navigation/stack";
/**
 * HostStackParams types definition
 */
export type HostStackParams = {
  [HostStackScreens.HostList]: undefined;
  [HostStackScreens.HostDetail]: { rowId: string } | undefined;
  [HostStackScreens.ServiceList]: undefined;
};
type HostScreenKeys = keyof HostStackParams;

type HostStackRoutesProps<ScreenName extends HostScreenKeys> = RouteProp<HostStackParams, ScreenName>;

export type HostNavigationProps<ScreenName extends HostScreenKeys> = StackNavigationProp<HostStackParams, ScreenName>;
export type HostStackScreenProps<ScreenName extends HostScreenKeys> = {
  route: HostStackRoutesProps<ScreenName>;
  navigation: HostNavigationProps<ScreenName>;
};
export enum HostStackScreens {
  HostList = "HostList",
  HostDetail = "HostDetail",
  ServiceList = "ServiceList",
}
