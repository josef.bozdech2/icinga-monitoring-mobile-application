import { RouteProp } from "@react-navigation/core";
import { StackNavigationProp } from "@react-navigation/stack";
/**
 * ServiceStackParams types definition
 */
export type ModalFormTypes = { name?: string };
export type ServiceStackParams = {
  [ServiceStackScreens.ServiceDetail]: { rowId: string } | undefined;
  [ServiceStackScreens.HostDetail]: undefined;
  [ServiceStackScreens.ServiceList]: { filterValue: string } | undefined ;
};
type ServiceScreenKeys = keyof ServiceStackParams;

type ServiceStackRoutesProps<ScreenName extends ServiceScreenKeys> = RouteProp<ServiceStackParams, ScreenName>;

export type ServiceNavigationProps<ScreenName extends ServiceScreenKeys> = StackNavigationProp<ServiceStackParams, ScreenName>;
export type ServiceStackScreenProps<ScreenName extends ServiceScreenKeys> = {
  route: ServiceStackRoutesProps<ScreenName>;
  navigation: ServiceNavigationProps<ScreenName>;
};
export enum ServiceStackScreens {
  ServiceDetail = "ServiceDetail",
  HostDetail = "HostDetail",
  ServiceList = "ServiceList",
}
