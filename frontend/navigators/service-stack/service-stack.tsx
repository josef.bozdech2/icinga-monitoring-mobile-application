import { createStackNavigator } from "@react-navigation/stack";
import { Spinner } from "native-base";
import React, { useContext, useEffect } from "react";
import AppContext from "../../contexts/app-context";
import { getIsOSVersionValid } from "../../helpers/getIsOSVersionValid";
import useNotifications from "../../hooks/use-notifications";
import { useTokenRegistration } from "../../hooks/use-token-registration";
import HostDetail from "../../screens/host-detail";
import ServiceDetail from "../../screens/service-detail";
import ServiceList from "../../screens/service-list";
import { ServiceStackParams, ServiceStackScreens } from "./types";

const ServiceStack = createStackNavigator<ServiceStackParams>();
/**
 * Navigator for clicking through service stack
 * @returns ServiceStackNavigator
 */
export const ServiceStackNavigator = () => {
  const { setIsValidOS, isSubscribed } = useContext(AppContext)!;
  const { expoPushToken } = useNotifications();
  if(expoPushToken)
    useTokenRegistration(expoPushToken);

  useEffect(() => {
    setIsValidOS(getIsOSVersionValid());
  }, []);

  if (!isSubscribed) return <Spinner />;

  return (
    <ServiceStack.Navigator screenOptions={{ headerShown: false }}>
      <ServiceStack.Screen name={ServiceStackScreens.ServiceList} component={ServiceList} />
      <ServiceStack.Screen name={ServiceStackScreens.ServiceDetail} component={ServiceDetail} />
      <ServiceStack.Screen name={ServiceStackScreens.HostDetail} component={HostDetail} />
    </ServiceStack.Navigator>
  );
};
