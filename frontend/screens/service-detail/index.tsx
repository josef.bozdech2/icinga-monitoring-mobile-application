import React, { useContext } from "react";
import { List, Spinner, Container, Content } from "native-base";
import useData from "../../hooks/use-data";
import AppContext from "../../contexts/app-context";
import { DetailHeader } from "../../components/app-headers";

import SectionPluginOutput from "../../components/entity-sections/plugin-output";
import SectionProblemHandling from "../../components/entity-sections/problem-handling";
import SectionPerformanceData from "../../components/entity-sections/performance-data";
import SectionNotifications from "../../components/entity-sections/notifications";
import SectionCheckExecution from "../../components/entity-sections/check-execution";
import SectionFeatureCommands from "../../components/entity-sections/feature-commands";
import SectionExistingComments from "../../components/entity-sections/existing-comments";
import SectionScheduledDowntimes from "../../components/entity-sections/scheduled-downtimes";
import SectionMoveToHost from "../../components/entity-sections/move-to-host";
import RefreshControllerComponent from "../../components/refresh-controller-component";
import ConfigContext from "../../contexts/config-context";
import { RowObject } from "../../interfaces/icinga.interface";
import { ServiceStackScreenProps, ServiceStackScreens } from "../../navigators/service-stack/types";

export interface HostAndServiceDetailUseDataProps {
  data: RowObject;
  onRefresh: () => {};
  isLoading: boolean;
}
type ServiceDetailScreenProps = ServiceStackScreenProps<ServiceStackScreens.ServiceDetail>;
/**
 * 
 * @param param0 object containing route and navigation parameters
 * @returns ServiceDetail screen component
 */
export default function ServiceDetail({ route, navigation }: ServiceDetailScreenProps) {
  const appContext = useContext(AppContext)!;
  const { config } = useContext(ConfigContext)!;

  const { data, isLoading, onRefresh }: HostAndServiceDetailUseDataProps = useData({
    config,
    url: `/icingaProxy/service/detail`,
    dtoIn: { expoPushToken: appContext?.expoPushToken, serviceId: route?.params?.rowId },
    refreshInterval: 30000,
  });
  const _sectionProps = { data, onRefresh };
  // if (isLoading) return <Spinner />;
  return (
    <Container>
      <RefreshControllerComponent onRefresh={onRefresh}>
        <Content>
          <DetailHeader
            navigation={navigation}
            title="Service Detail"
            subtitle={route?.params?.rowId || ""}
            status={data?.attrs?.state}
            type="service"
          />
          {isLoading?
            <Spinner />: 
            <List>
              <SectionMoveToHost {..._sectionProps} />
              <SectionPluginOutput {..._sectionProps} />
              <SectionCheckExecution {..._sectionProps} />
              <SectionProblemHandling {..._sectionProps} />
              <SectionNotifications {..._sectionProps} />
              <SectionExistingComments {..._sectionProps} />
              <SectionScheduledDowntimes {..._sectionProps} />
              <SectionPerformanceData {..._sectionProps} />
            </List>}
        </Content>
      </RefreshControllerComponent>
    </Container>
  );
}
