import React, { useContext, useState } from "react";
import { Container, Spinner } from "native-base";
import useData from "../../hooks/use-data";
import List from "../../components/big-list/list";
import AppContext from "../../contexts/app-context";
import { OverviewHeader } from "../../components/app-headers";
import { BigListFilterHeader } from "../../components/big-list/header";
import { RowObject } from "../../interfaces/icinga.interface";
import ConfigContext, { ConfigContextType } from "../../contexts/config-context";
import { AuthStackScreenProps, AuthStackScreens } from "../../navigators/auth-stack/types";
import { HostStackScreenProps, HostStackScreens } from "../../navigators/host-stack/types";
/**
 * 
 * @param param0 object containing navigation
 * @returns HostList screen component
 */
 type HostListScreenProps = HostStackScreenProps<HostStackScreens.HostList>;

export default function HostList({ navigation }: HostListScreenProps) {
  const appContext = useContext(AppContext);
  const { config } = useContext(ConfigContext)!;

  const [filterValue, setFilterValue] = useState<string | null>(null);
  const [picker, setPicker] = useState("-1");

  const { data, isLoading, fetchData, onRefresh } = useData({
    config,
    url: "/icingaProxy/host/list",
    dtoIn: { expoPushToken: appContext?.expoPushToken, ...getSearchValue() },
    refreshInterval: 30000,
  });

  const handleFilterChange = (value: string) => {
    fetchData({ expoPushToken: appContext?.expoPushToken, ...getSearchValue(value) });
    setFilterValue(value);
  };

  function getSearchValue(_filterValue: string | null = filterValue) {
    if (!_filterValue) return {};
    return { host: _filterValue };
  }

  if (isLoading)
    return (
      <Container>
        <OverviewHeader navigation={navigation} title="Host List" />
        <BigListFilterHeader {...{ objectType: "Host", handleFilterChange, filterValue, picker, handlePickerChange: setPicker }} />
        <Spinner />
      </Container>
    );

  // eslint-disable-next-line no-nested-ternary
  const _data: RowObject[] = data?.results?.filter((item: RowObject) =>
    picker === "0"
      ? true
      : picker === "-1"
      ? item?.attrs?.state == "1.0" || item?.attrs?.state == "2.0" || item?.attrs?.state == "3.0"
      : item?.attrs?.state == picker
  );

  return (
    <Container>
      <OverviewHeader navigation={navigation} title="Host List" />

      <BigListFilterHeader {...{ objectType: "Host", handleFilterChange, filterValue, picker, handlePickerChange: setPicker }} />
      <List
        data={_data}
        onRefresh={onRefresh}
        itemHeight={70}
        onRowClick={(row: RowObject) => navigation.navigate(HostStackScreens.HostDetail, { rowId: row.name })}
      />
    </Container>
  );
}
