import Axios from "axios";
import { reloadAsync } from "expo-updates";
import { Button, Container, Content, Icon, Right, Text, Toast } from "native-base";
import React, { useState } from "react";
import { OverviewHeader } from "../../components/app-headers";
import ServerContext, { ServerObjectType } from "../../contexts/server-context";
import { NotAuthStackScreenProps, NotAuthStackScreens } from "../../navigators/not-auth-stack/types";
import ServerSelectForm from "./server-select-form";

type ServerSelectScreenProps = NotAuthStackScreenProps<NotAuthStackScreens.ServerSelectScreen>;
/**
 * 
 * @param param0 ServerSelectScreenProps object
 * @returns ServerSelectScreen screen
 */
const ServerSelectScreen = ({ navigation }: ServerSelectScreenProps) => {
  const { serverConfig, setServer } = React.useContext(ServerContext)!;
  const [serverState, setServerState] = useState<Pick<ServerObjectType, "host" | "port" | "password" | "username">>({
    host: serverConfig.host || "",
    port: serverConfig.port || "",
    username: serverConfig.username,
    password: serverConfig.password,
  });

  async function handleSubmit() {
    try {
      Toast.show({
        text: "Testing server connection",
        position: "bottom",
        type: "warning",
      });

      const port = serverState.port ? ":" + serverState.port : "";
      await Axios.get(`http://${serverState.host}${port}/`, { timeout: 4000 });
      await setServer({ ...serverState, isServerConnectionValid: true });

      reloadAsync();
    } catch (error) {
      console.log({ error });
      Toast.show({
        text: "Server connection is invalid",
        position: "bottom",
        type: "danger",
      });
    }
  }
  function handleChange(_serverConfig: Partial<ServerObjectType>) {
    setServerState({ ...serverState, ..._serverConfig });
  }

  return (
    <Container>
      <Content>
        <OverviewHeader
          navigation={navigation}
          title="Backend config"
          rightToolbar={
            <Right>
              {
                <Button small block onPress={handleSubmit} success>
                  <Icon name="save" type="MaterialIcons" />
                  <Text>Connect</Text>
                </Button>
              }
            </Right>
          }
        />
        <ServerSelectForm serverConfig={serverState} handleChange={handleChange}></ServerSelectForm>
      </Content>
    </Container>
  );
};

export default ServerSelectScreen;
