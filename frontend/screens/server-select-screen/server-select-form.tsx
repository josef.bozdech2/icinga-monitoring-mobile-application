import { Container, Form, Input } from "native-base";
import React from "react";
import appJson from "../../app.json";
import { OverviewHeader } from "../../components/app-headers";
import { AppInputWrapper } from "../../components/app-input-wrapper/app-input-wrapper";
import { ServerObjectType } from "../../contexts/server-context";

type ServerSelectFormProps = {
  serverConfig: Pick<ServerObjectType, "host" | "port" | "password" | "username">;
  handleChange: (serverConfig: Partial<ServerObjectType>) => void;
};
/**
 * 
 * @param param0 ServerSelectFormProps object
 * @returns ServerSelectForm component
 */
const ServerSelectForm = ({ serverConfig, handleChange }: ServerSelectFormProps) => {
  return (
    <Form>
      <AppInputWrapper label="Host">
        <Input value={serverConfig.host} onChange={(e) => handleChange({ host: e.nativeEvent.text })} />
      </AppInputWrapper>
      <AppInputWrapper label="Port">
        <Input value={serverConfig.port} onChange={(e) => handleChange({ port: e.nativeEvent.text })} />
      </AppInputWrapper>
      {/* <AppInputWrapper label="Username">
        <Input value={serverConfig.username} onChange={(e) => handleChange({ username: e.nativeEvent.text })} />
      </AppInputWrapper>
      <AppInputWrapper label="Password">
        <Input value={serverConfig.password} onChange={(e) => handleChange({ password: e.nativeEvent.text })} />
      </AppInputWrapper> */}
    </Form>
  );
};

export default ServerSelectForm;
