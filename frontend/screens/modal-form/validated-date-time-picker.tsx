import React, { useContext, useState } from "react";
import { Icon, Text, View } from "native-base";
import DateTimePicker from "@react-native-community/datetimepicker";
import moment from "moment";
import ValidateContext from "./validate-context";

enum ModeTypeEnum {
  time = "time",
  date = "date",
}
const MODE_ENUM = {
  [ModeTypeEnum.time]: (value: Date) => moment(value).format("HH:mm"),
  [ModeTypeEnum.date]: (value: Date) => moment(value).format("MMM Do YY"),
};
/**
 *
 * @param param0 object containing parameters for DateTimePicker
 * @returns ValidatedDateTimePicker
 */
export interface ValidatedDateTimePickerProps {
  value: Date;
  onValidate: (date: string) => boolean;
  onValueChange: (value: string) => void;
  inputKey: string;
  passThroughProps: { mode?: string; is24Hour?: boolean };
}
function ValidatedDateTimePicker({ value, onValidate, onValueChange, inputKey, passThroughProps = {} }: ValidatedDateTimePickerProps) {
  const { validationMap, setIsValid } = useContext(ValidateContext)!;
  const isValid = validationMap[inputKey];

  const [isPickerShown, setIsPickerShown] = useState(false);

  function handleChange(_value: string) {
    setIsPickerShown(false);
    if (!_value) return;
    onValueChange(_value);

    if (onValidate && onValidate(_value)) {
      setIsValid(inputKey, true);
    } else setIsValid(inputKey, false);
  }

  const mode = (passThroughProps.mode as ModeTypeEnum) || ModeTypeEnum.time;

  return (
    <View
      key={inputKey + isValid}
      style={{ display: "flex", flexDirection: "row", justifyContent: "space-between", width: 150, marginRight: 20 }}>
      <Text onPress={() => !isPickerShown && setIsPickerShown(true)}>{MODE_ENUM[mode](value)}</Text>
      {isPickerShown && (
        <DateTimePicker {...passThroughProps} value={value || new Date()} onChange={(e: any, _value: any) => handleChange(_value)} />
      )}

      {isValid === false && <Icon style={{color: "red"}} name="close-circle" />}
      {isValid === true && <Icon style={{color: "green"}} name="checkmark-circle" />}
    </View>
  );
}

export default React.memo(ValidatedDateTimePicker);
