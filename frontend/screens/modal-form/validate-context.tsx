import React from "react";
/**
 * ValidateContext for ModalForm
 */
export type ValidateContextWrapperType = {
    validationMap: {
        [key: string]: boolean;
      };
    isAllValid: boolean;
    setIsValid: (key: string, value: any) => void;
  };
const ValidateContext = React.createContext<ValidateContextWrapperType | null>(null);
export default ValidateContext;
