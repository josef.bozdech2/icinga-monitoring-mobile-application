import React, { useContext } from "react";
import { Icon, Item, Label, Textarea } from "native-base";
import { StyleSheet } from "react-native";
import ValidateContext from "./validate-context";
import { ValidatedObjectProps } from ".";

export interface ValidatedTextAreaProps extends ValidatedObjectProps {
  onValidate: (value: string) => string;
  onChange: (value: string) => void;
  style: {};
  rowSpan: number;
  bordered: boolean;
  value: any;
}
/**
 *
 * @param param0 object for ValidatedTextArea rendering
 * @returns ValidatedTextArea component
 */
export default function ValidatedTextArea({ onValidate, onChange, label, inputKey, ...passProps }: ValidatedTextAreaProps) {
  const { validationMap, setIsValid } = useContext(ValidateContext)!;
  const isValid = validationMap[inputKey];

  function handleChange(_value: string) {
    onChange(_value);
    if (onValidate && onValidate(_value)) {
      setIsValid(inputKey, true);
    } else setIsValid(inputKey, false);
  }

  const _props = { required: true, ...passProps };

  return (
    <Item style={styles.itemContainer} success={isValid === true} error={isValid === false}>
      <Label>{label}</Label>
      <Textarea {..._props} onChange={(e) => handleChange(e.nativeEvent.text)} rowSpan={1} bordered={false} underline={false} />
      {isValid === false && <Icon name="close-circle" />}
      {isValid === true && <Icon name="checkmark-circle" />}
    </Item>
  );
}

const styles = StyleSheet.create({
  itemContainer: {
    paddingTop: 4,
    paddingBottom: 4,
    display: "flex",
    justifyContent: "space-between",
    flexDirection: "row",
  },
});
