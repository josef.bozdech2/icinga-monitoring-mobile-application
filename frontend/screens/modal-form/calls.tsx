import Axios from "axios";
import moment from "moment";
import { Toast } from "native-base";
import { AppContextWrapperType } from "../../contexts/app-context";
import { AttrsObject } from "../../interfaces/icinga.interface";
/**
 * functions for POST api calls
 */
export interface ScheduleDowntimeParams {
  type: string;
  comment: string;
  start_time: Date;
  start_date: Date;
  end_time: Date;
  end_date: Date;
  fixed: "fixed" | "flexible";
  duration_hours: number;
  duration_minutes: number;
  all_services: boolean;
  child_options: number;
}

export interface AcknowledgeParams {
  comment: string;
  persistent: boolean;
  use_expiry: boolean | undefined;
  expiry_date: Date | undefined;
  expiry_time: Date | undefined;
  sticky: boolean | undefined;
  notify: boolean | undefined;
}

export interface AddCommentParams {
  comment: string;
  author: string;
}

export interface SendCustomNotificationParams {
  type: string;
  comment: string;
  use_expiry: boolean | undefined;
}
interface GeneralActionType {
  type?: string;
  filter?: string;
  author?: string;
  comment?: string;
}
interface AddCommentType extends GeneralActionType {}
interface AcknowledgeType extends GeneralActionType {
  sticky?: boolean;
  notify?: boolean;
  persistent?: boolean;
  expiry?: string;
}

interface ScheduleDowntimeType extends GeneralActionType {
  all_services?: boolean;
  child_options?: number;
  fixed?: boolean;
  duration?: number;
  start_time?: string;
  end_time?: string;
}

export enum ModalFormCallsEnum {
  acknowledge = "acknowledge",
  addComment = "addComment",
  scheduleDowntime = "scheduleDowntime",
  sendCustomNotification = "sendCustomNotification",
}

export const modalFormCallsMap = {
  // DONE
  [ModalFormCallsEnum.addComment]: (
    { author = "root", comment }: AddCommentParams,
    appContext: AppContextWrapperType,
    attrs: AttrsObject,
    buildKiviciUrl: (path: string) => string
  ) => {
    const _url = "/icingaProxy/actions/addComment";
    let _comment: AddCommentType = { author, comment: typeof comment !== "undefined" ? comment : " " };

    if (attrs.type === "Service") {
      _comment = {
        ..._comment,
        type: "Service",
        filter: `match(\"${attrs.host_name}\", host.name)&&match(\"${attrs.display_name}\", service.name)`,
      };
    } else {
      _comment = {
        ..._comment,
        type: "Host",
        filter: `match(\"${attrs.display_name}\", host.name)`,
      };
    }
    Axios.post(buildKiviciUrl(_url), { expoPushToken: appContext.expoPushToken, params: _comment });
  },
  // ADD TIMESTAMP
  [ModalFormCallsEnum.acknowledge]: (
    { comment, use_expiry, sticky, notify, persistent, expiry_date, expiry_time }: AcknowledgeParams,
    appContext: AppContextWrapperType,
    attrs: AttrsObject,
    buildKiviciUrl: (path: string) => string
  ) => {
    const _url = "/icingaProxy/actions/acknowledgeProblem";
    let _comment: AcknowledgeType = {
      author: "root",
      comment: typeof comment !== "undefined" ? comment : " ",
      sticky: typeof sticky !== "undefined" ? sticky : false,
      notify: typeof notify !== "undefined" ? notify : false,
      persistent: typeof persistent !== "undefined" ? persistent : false,
    };
    if (typeof use_expiry !== "undefined") {
      if (typeof expiry_date !== "undefined" && typeof expiry_time !== "undefined") {
        const _expiry_date = new Date(expiry_date);
        const _expiry_time = new Date(expiry_time);
        const _expiry_date_days = _expiry_date.getDate();
        const _expiry_date_months = _expiry_date.getMonth();
        const _expiry_date_years = _expiry_date.getFullYear();
        const _expiry_time_hours = _expiry_time.getHours();
        const _expiry_time_minutes = _expiry_time.getMinutes();
        const _expiry_full_date = new Date(
          _expiry_date_years,
          _expiry_date_months,
          _expiry_date_days,
          _expiry_time_hours,
          _expiry_time_minutes
        );
        const expiry: string = moment(_expiry_full_date).format("X");
        _comment = { ..._comment, expiry };
      } else {
        Toast.show({
          text: "Expiry not set, setting to current time!",
          buttonText: "Okay",
          position: "bottom",
          style: { bottom: "50%" },
        });
        const expiry = moment(new Date()).format("X");
        _comment = { ..._comment, expiry };
      }
    }

    if (attrs.type === "Service") {
      _comment = {
        ..._comment,
        type: "Service",
        filter: `match(\"${attrs.host_name}\", host.name)&&match(\"${attrs.display_name}\", service.name)`,
      };
    } else {
      _comment = {
        ..._comment,
        type: "Host",
        filter: `match(\"${attrs.display_name}\", host.name)`,
      };
    }
    Axios.post(buildKiviciUrl(_url), { expoPushToken: appContext.expoPushToken, params: _comment });
  },

  [ModalFormCallsEnum.scheduleDowntime]: (
    {
      type,
      comment,
      start_time,
      start_date,
      end_time,
      end_date,
      fixed,
      duration_hours,
      duration_minutes,
      all_services,
      child_options,
    }: ScheduleDowntimeParams,
    appContext: AppContextWrapperType,
    attrs: AttrsObject,
    buildKiviciUrl: (path: string) => string
  ) => {
    const _url = "/icingaProxy/actions/scheduleDowntime";
    /* START AND END TIME MOMENT */

    const _start_date = new Date(start_date);
    const _start_time = new Date(start_time);
    const _start_date_days = _start_date.getDate();
    const _start_date_months = _start_date.getMonth();
    const _start_date_years = _start_date.getFullYear();
    const _start_time_hours = _start_time.getHours();
    const _start_time_minutes = _start_time.getMinutes();
    const _start_full_date = new Date(_start_date_years, _start_date_months, _start_date_days, _start_time_hours, _start_time_minutes);
    const start_timestamp = moment(_start_full_date).format("X");

    const _end_date = new Date(end_date);
    const _end_time = new Date(end_time);
    const _end_date_days = _end_date.getDate();
    const _end_date_months = _end_date.getMonth();
    const _end_date_years = _end_date.getFullYear();
    const _end_time_hours = _end_time.getHours();
    const _end_time_minutes = _end_time.getMinutes();
    const _end_full_date = new Date(_end_date_years, _end_date_months, _end_date_days, _end_time_hours, _end_time_minutes);
    const end_timestamp = moment(_end_full_date).format("X");

    let _comment: ScheduleDowntimeType = {
      type,
      author: "root",
      comment: typeof comment !== "undefined" ? comment : " ",
      start_time: start_timestamp,
      end_time: end_timestamp,
    };
    if (fixed === "flexible") {
      if (duration_hours && duration_minutes) {
        _comment = { ..._comment, fixed: false, duration: duration_hours * 3600 + duration_minutes * 60 };
      } else {
        _comment = { ..._comment, fixed: false, duration: 0 };
      }
    }
    if (attrs.type === "Service") {
      _comment = {
        ..._comment,
        type: "Service",
        filter: `match(\"${attrs.host_name}\", host.name)&&match(\"${attrs.display_name}\", service.name)`,
      };
    } else {
      _comment = {
        ..._comment,
        type: "Host",
        filter: `match(\"${attrs.display_name}\", host.name)`,
        all_services,
        child_options,
      };
    }
    Axios.post(buildKiviciUrl(_url), { expoPushToken: appContext.expoPushToken, params: _comment });
  },
  [ModalFormCallsEnum.sendCustomNotification]: (
    { type, comment }: SendCustomNotificationParams,
    appContext: AppContextWrapperType,
    attrs: AttrsObject,
    buildKiviciUrl: (path: string) => string
  ) => {
    const _url = "/icingaProxy/actions/sendCustomNotification";
    let _comment: GeneralActionType = { comment, author: "root" };
    if (attrs.type === "Service") {
      _comment = {
        ..._comment,
        type: "Service",
        filter: `match(\"${attrs.host_name}\", host.name)&&match(${attrs.display_name}, service.name)`,
      };
    } else {
      _comment = {
        ..._comment,
        type: "Host",
        filter: `match(\"${attrs.display_name}\", host.name)`,
      };
    }
    Axios.post(buildKiviciUrl(_url), { expoPushToken: appContext.expoPushToken, params: _comment });
  },
  // removeDowntime: ({ type }, appContext, attrs) => {
  //   const _url = "/icingaProxy/actions/removeDowntime";
  //   let _comment = { type };
  //   if (attrs.type === "Service") {
  //     _comment = {
  //       ..._comment,
  //       type: "Service",
  //       filter: `match(\"${attrs.host_name}\", host.name)&&match(\"${attrs.display_name}\", service.name)`,
  //     };
  //   } else {
  //     _comment = {
  //       ..._comment,
  //       type: "Host",
  //       filter: `match(\"${attrs.display_name}\", host.name)`,
  //     };
  //   }
  //   Axios.post(buildKiviciUrl(_url), { expoPushToken: appContext.expoPushToken, params: _comment });
  // },
  // removeAcknowledge: (appContext, attrs) => {
  //   const _url = "/icingaProxy/actions/removeProblem";
  //   let _comment = null;
  //   if (attrs.type === "Service") {
  //     _comment = {
  //       type: "Service",
  //       filter: `match(\"${attrs.host_name}\", host.name)&&match(\"${attrs.display_name}\", service.name)`,
  //     };
  //   } else {
  //     _comment = {
  //       type: "Host",
  //       filter: `match(\"${attrs.display_name}\", host.name)`,
  //     };
  //   }
  //   Axios.post(buildKiviciUrl(_url), { expoPushToken: appContext.expoPushToken, _comment });
  // },
  // getDowntimes: ({ type }, appContext, attrs) => {
  //   const _url = "/icingaProxy/objects/downtimes";
  //   let _comment = { type };
  //   if (attrs.type === "Service") {
  //     _comment = {
  //       type: "Service",
  //       filter: `match(\"${attrs.host_name}\", host.name)&&match(\"${attrs.display_name}\", service.name)`,
  //     };
  //   } else {
  //     _comment = {
  //       type: "Host",
  //       filter: `match(\"${attrs.display_name}\", host.name)`,
  //     };
  //   }
  //   Axios.get(buildKiviciUrl(_url), { expoPushToken: appContext.expoPushToken, params: _comment });
  // },
  // DONE
  // getComments: ({ type }: AttrsObject, appContext: AppContextWrapperType, attrs: AttrsObject) => {
  //   const _url = "/icingaProxy/objects/comments";
  //   let _comment = {};
  //   if (attrs.type === "Service") {
  //     _comment = {
  //       type: "Service",
  //       filter: `match(\"${attrs.host_name}\", host.name)&&match(\"${attrs.display_name}\", service.name)`,
  //     };
  //   } else {
  //     _comment = {
  //       type: "Host",
  //       filter: `match(\"${attrs.display_name}\", host.name)`,
  //     };
  //   }
  //   Axios.get(buildKiviciUrl(_url), { expoPushToken: appContext?.expoPushToken, params: _comment });
  // },
};
