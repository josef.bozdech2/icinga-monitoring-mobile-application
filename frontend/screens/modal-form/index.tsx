import moment from "moment";
import { Button, Container, Content, Form, Icon, Input, Item, Label, Picker, Text, View } from "native-base";
import React, { useContext, useEffect, useRef, useState } from "react";
import { Alert, StyleSheet } from "react-native";
import { Switch } from "react-native-gesture-handler";
import AppContext from "../../contexts/app-context";
import { useBuildKiviciUrl } from "../../hooks/use-build-kivici-url";
import { AttrsObject } from "../../interfaces/icinga.interface";
import { RootStackScreenProps, RootStackScreens } from "../../navigators/root-stack/types";
import { modalFormCallsMap } from "./calls";
import ValidateContext from "./validate-context";
import ValidateContextProvider from "./validate-context-provider";
import ValidatedDateTimePicker from "./validated-date-time-picker";
import ValidatedInput from "./validated-input";
import ValidatedTextArea from "./validated-text-area";

/**
 *
 * @param param0 object containing route and navigation objects
 * @returns ModalForm components
 */
export interface RouteProps {
  params: RouteParams;
}

export interface RouteParams {
  attrs: AttrsObject;
  onRefresh: () => void;
  formItems: Array<FormItems>;
}

export interface ValidatedObjectProps {
  label: string;
  inputKey: string;
}
export interface FormItems {
  key: string;
  label: string;
  type: string;
  initialValidation?: boolean;
  items?: [{}];
}

type ModalFormProps = RootStackScreenProps<RootStackScreens.ModalForm>;

export const ModalForm = ({ navigation, route }: ModalFormProps) => {
  const appContext = useContext(AppContext)!;
  const [state, setState] = useState<{ [key: string]: any }>({});

  const { buildKiviciUrl } = useBuildKiviciUrl();

  const { attrs, formItems, formSubmitCall, onRefresh } = route.params;
  const hasChangesRef = useRef(false);

  useEffect(
    () =>
      navigation.addListener("beforeRemove", (e: { preventDefault: () => void; data: { action: any } }) => {
        if (!hasChangesRef.current) {
          return;
        }
        e.preventDefault();
        Alert.alert("Discard changes?", "You have unsaved changes. Are you sure to discard them and leave the screen?", [
          { text: "Don't leave", style: "cancel", onPress: () => {} },
          {
            text: "Discard",
            style: "destructive",
            onPress: () => navigation.dispatch(e.data.action),
          },
        ]);
      }),
    [navigation, state]
  );

  function handleChange(value: any, key: string) {
    console.log(key);
    console.log(value);
    setState({ ...state, [key]: value });
    hasChangesRef.current = true;
  }

  async function handleSubmit() {
    if (!formSubmitCall || !modalFormCallsMap[formSubmitCall]) throw new Error("ModalForm => formSubmitCall is not defined");
    hasChangesRef.current = false;
    try {
      await modalFormCallsMap[formSubmitCall](state, appContext, attrs, buildKiviciUrl);
      if (onRefresh) await onRefresh();
      navigation.goBack();
    } catch (error) {
      Alert.alert("Data cannot be saved", `Error occurred during saving`);
    }
  }

  function renderInput({ key, label, type, items }: { key: string; label: string; type: string; items?: any[] }) {
    if (type === "NUMBER_INPUT") {
      return (
        <ValidatedInput
          key={key}
          inputKey={key}
          label={label}
          value={state[key]}
          onChange={(value) => handleChange(value, key)}
          onValidate={(value: number) => value && Number.isInteger(Number(value))}
        />
      );
    }
    if (type === "TEXTAREA") {
      return (
        <ValidatedTextArea
          style={{ width: "65%", margin: "2%" }}
          rowSpan={5}
          bordered
          key={key}
          inputKey={key}
          label={label}
          value={state[key]}
          onChange={(value: string) => handleChange(value, key)}
          onValidate={(value: string) => value}
        />
      );
    }
    if (type === "DATE") {
      return (
        <ValidatedInput
          key={key}
          inputKey={key}
          label={label}
          value={state[key]}
          onChange={(value) => handleChange(value, key)}
          onValidate={validateDate}
        />
      );
    }
    return (
      <Item key={key} style={styles.itemContainer}>
        <Label>{label}</Label>
        {type === "TEXT" && <Input value={state[key]} onChange={(e) => handleChange(e.nativeEvent.text, key)} />}
        {type === "SWITCH" && <Switch value={state[key]} onChange={(e) => handleChange(!state[key] || false, key)} />}
        {type === "SELECT" && (
          <Picker
            mode="dropdown"
            iosIcon={<Icon name="arrow-down" />}
            style={{ width: undefined }}
            selectedValue={state[key]}
            onValueChange={(value: any) => handleChange(value, key)}>
            {items?.map((item) => (
              <Picker.Item {...item} />
            ))}
          </Picker>
        )}
        {type === "DATE_PICKER" && (
          <ValidatedDateTimePicker
            key={key}
            inputKey={key}
            value={state[key]}
            onValueChange={(value) => handleChange(value, key)}
            passThroughProps={{ mode: "date" }}
            onValidate={validateDate}
          />
        )}

        {type === "TIME_PICKER" && (
          <ValidatedDateTimePicker
            key={key}
            inputKey={key}
            value={state[key]}
            onValueChange={(value) => handleChange(value, key)}
            passThroughProps={{ mode: "time", is24Hour: true }}
            onValidate={validateDate}
          />
        )}
      </Item>
    );
  }

  const initialValidationMap: { [key: string]: boolean } = {};

  formItems.forEach((item) => {
    if (item.initialValidation) initialValidationMap[item.key] = false;
  });

  return (
    <Container>
      <ValidateContextProvider initialValidationMap={initialValidationMap}>
        <ValidateContext.Consumer>
          {(validateContext) => (
            <Content>
              <Form>
                {formItems?.map((item) => renderInput(item))}

                <View style={styles.buttonContainer}>
                  {!validateContext?.isAllValid && (
                    <Button block warning disabled>
                      <Icon name="save" type="MaterialIcons" />
                      <Text>Submit</Text>
                    </Button>
                  )}
                  {validateContext?.isAllValid && (
                    <Button block onPress={handleSubmit}>
                      <Icon name="save" type="MaterialIcons" />
                      <Text>Submit</Text>
                    </Button>
                  )}
                </View>
              </Form>
            </Content>
          )}
        </ValidateContext.Consumer>
      </ValidateContextProvider>
    </Container>
  );
};

const styles = StyleSheet.create({
  buttonContainer: {
    marginTop: 24,
    display: "flex",
    justifyContent: "center",
    flexDirection: "row",
  },
  itemContainer: {
    paddingBottom: 4,
    paddingTop: 4,

    display: "flex",
    justifyContent: "space-between",
    flexDirection: "row",
  },
});

function validateDate(date: string) {
  return moment(date, "h:m:s D.M.YYYY", true).isValid();
}
