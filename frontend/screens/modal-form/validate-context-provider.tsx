import React, { useState } from "react";
import ValidateContext from "./validate-context";
/**
 *
 * @param param0 object containing children and initial validation map for item validation
 * @returns ValidationContextProvider
 */
export interface ValidateContextProviderProps {
  children: React.ReactNode;
  initialValidationMap: {
    [key: string]: boolean;
  };
}
function ValidateContextProvider({ children, initialValidationMap }: ValidateContextProviderProps) {
  const [validationMap, setValidationMap] = useState(initialValidationMap || {});

  function setIsValid(key: string, value: boolean) {
    setValidationMap({ ...validationMap, [key]: value === true });
  }

  return (
    <ValidateContext.Provider
      value={{
        validationMap,
        isAllValid: !Object.keys(validationMap).find((key) => validationMap[key] === false),
        setIsValid,
      }}>
      {children}
    </ValidateContext.Provider>
  );
}

export default ValidateContextProvider;
