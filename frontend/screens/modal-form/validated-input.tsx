import React, { useContext } from "react";
import { Icon, Input, Item, Label } from "native-base";
import { StyleSheet } from "react-native";
import ValidateContext from "./validate-context";
import { ValidatedObjectProps } from ".";
/**
 * 
 * @param param0 object for validated input rendering
 * @returns ValidatedInput component
 */
 export interface ValidatedInputProps extends ValidatedObjectProps {
  onValidate: any;
  onChange: (value: number | Date) => void;
  value: any;
} 
export default function ValidatedInput({ onValidate, onChange, label, inputKey, ...passProps }: ValidatedInputProps) {
  const { validationMap, setIsValid } = useContext(ValidateContext)!;
  const isValid = validationMap[inputKey];

  function handleChange(_value: any) {
    onChange(_value);
    if (onValidate && onValidate(_value)) {
      setIsValid(inputKey, true);
    } else setIsValid(inputKey, false);
  }

  const _props = { required: true, ...passProps };

  return (
    <Item style={styles.itemContainer} success={isValid === true} error={isValid === false}>
      <Label>{label}</Label>
      <Input {..._props} onChange={(e) => handleChange(e.nativeEvent.text)} />
      {isValid === false && <Icon name="f" />}
      {isValid === true && <Icon name="checkmark-circle" />}
    </Item>
  );
}

const styles = StyleSheet.create({
  itemContainer: {
    paddingTop: 4,
    paddingBottom: 4,
    display: "flex",
    justifyContent: "space-between",
    flexDirection: "row",
  },
});
