import React, { useContext } from "react";
import { Container, Content } from "native-base";
import appJson from "../../app.json";
import AppContext from "../../contexts/app-context";
import OverviewComponent from "../../components/overview-component";
import { OverviewHeader } from "../../components/app-headers";
import { AuthStackScreenProps, AuthStackScreens } from "../../navigators/auth-stack/types";

type OverviewScreenProps = AuthStackScreenProps<AuthStackScreens.OverviewScreen>;
/**
 *
 * @param param0 object containing navigation parameter
 * @returns OverviewScreen component
 */
const OverviewScreen = ({ navigation }: OverviewScreenProps) => {
  const { expoPushToken } = useContext(AppContext)!;

  return (
    <Container>
      <Content>
        <OverviewHeader navigation={navigation} title="KivIci Tool" subtitle={`v${appJson.expo.version}`} />
        {expoPushToken && <OverviewComponent/>}
      </Content>
    </Container>
  );
};

export default OverviewScreen;
