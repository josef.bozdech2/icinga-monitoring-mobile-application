import React, { useContext } from "react";
import { Icon, Input, Item, Label } from "native-base";
import { StyleSheet } from "react-native";
import ValidateContext from "./validate-context";

interface Props {
  onValidate(_value: string | number | boolean): any;
  onChange(_value: string | number | boolean): void;
  label?: string;
  inputKey: string | null;
  isChanged?: boolean;
  value?: string;
}
/**
 *
 * @param param0 object containing functions and parameters for proper input validation
 * @returns ValidatedInput component
 */
const ValidatedInput: React.FC<Props> = ({ onValidate, onChange, label, inputKey, isChanged, ...passProps }) => {
  const { validationMap, setIsValid } = useContext(ValidateContext);
  let isValid;
  if (inputKey) isValid = validationMap[inputKey];
/**
 * Setter for input
 * @param _value value to change
 */
  function handleChange(_value: string) {
    if (inputKey) {
      onChange(_value);
      if (onValidate && onValidate(_value)) {
        setIsValid(inputKey, true);
      } else setIsValid(inputKey, false);
    }
  }

  const _props = { required: true, ...passProps };

  return (
    <Item style={styles.itemContainer} success={isValid === true && isChanged} error={isValid === false && isChanged}>
      <Label>{label}</Label>
      <Input {..._props} onChange={(e) => handleChange(e.nativeEvent.text)} />
      {isValid === false && isChanged && <Icon name="close-circle" />}
      {isValid === true && isChanged && <Icon name="checkmark-circle" />}
    </Item>
  );
};

const styles = StyleSheet.create({
  itemContainer: {
    paddingTop: 4,
    paddingBottom: 4,
    display: "flex",
    justifyContent: "space-between",
    flexDirection: "row",
  },
});

export default ValidatedInput;
