import React from "react";
/**
 * ValidateContext for configscreen validation
 */
interface DefaultContextValue {
  setIsValid(inputKey: string, _value: string | number | boolean): any;
  validationMap: { [key: string]: any };
  isAllValid: boolean;
}

const defaultContextValue: DefaultContextValue = { validationMap: {}, setIsValid: () => {}, isAllValid: true };

const ValidateContext = React.createContext(defaultContextValue);
export default ValidateContext;
