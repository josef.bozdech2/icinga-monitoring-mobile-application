import { useEffect } from "react";
import { Alert } from "react-native";
import { CommonConfigScreenTypes } from "../types";

type UsePreventNavigationLeave = CommonConfigScreenTypes;
/**
 * function for validation if user wants to leave a filled form
 * @param param0 UsePreventNavigationLeave object
 */
export const usePreventNavigationLeave = ({ navigation, state, isChanged }: UsePreventNavigationLeave) => {
  useEffect(
    () =>
      navigation.addListener("beforeRemove", (e: { preventDefault: () => void; data: { action: any; }; }) => {
        if (!isChanged) return;
        e.preventDefault();
        Alert.alert("Discard changes?", "You have unsaved changes. Are you sure to discard them and leave the screen?", [
          { text: "Don't leave", style: "cancel", onPress: () => {} },
          {
            text: "Discard",
            style: "destructive",
            onPress: () => navigation.dispatch(e.data.action),
          },
        ]);
      }),
    [navigation, state]
  );
};
