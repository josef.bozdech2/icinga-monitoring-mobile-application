import { ConfigScreenEnum } from "./constants";
/**
 * Common types definition
 */
export type CommonConfigScreenTypes = {
  navigation: any;
  state: any;
  isChanged: boolean;
};

export type ConfigFormItemsType = { key: string | null; label: string; type: ConfigScreenEnum; props?: { mode: string } };
