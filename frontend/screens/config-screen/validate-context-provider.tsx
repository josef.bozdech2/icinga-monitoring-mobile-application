import React, { useState } from "react";
import ValidateContext from "./validate-context";

interface Props {
  initialValidationMap?: {};
}
/**
 * 
 * @param param0 children of context, validation map
 * @returns ValidateContext
 */
const ValidateContextProvider: React.FC<Props> = ({ children, initialValidationMap = {} }) => {
  const [validationMap, setValidationMap] = useState(initialValidationMap);

  function setIsValid(key: string, value: string | number | boolean) {
    setValidationMap({ ...validationMap, [key]: value === true });
  }

  return (
    <ValidateContext.Provider
      value={{
        validationMap,
        isAllValid: !Object.keys(validationMap).find((key) => validationMap[key] === false),
        setIsValid,
      }}>
      {children}
    </ValidateContext.Provider>
  );
};

export default ValidateContextProvider;
