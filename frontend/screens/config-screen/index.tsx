import Axios from "axios";
import { Button, Container, Content, Form, Icon, Input, Left, ListItem, Right, Switch, Text, Toast } from "native-base";
import React, { useContext, useState } from "react";
import { Platform } from "react-native";
import { OverviewHeader } from "../../components/app-headers";
import { AppInputWrapper } from "../../components/app-input-wrapper/app-input-wrapper";
import AppContext from "../../contexts/app-context";
import ConfigContext, { ConfigObjectType } from "../../contexts/config-context";
import { validateIPAddress } from "../../helpers/validateIPAddress";
import { useBuildKiviciUrl } from "../../hooks/use-build-kivici-url";
import { AuthStackScreenProps, AuthStackScreens } from "../../navigators/auth-stack/types";
import { shallowCompare } from "../../services";
import { ConfigScreenFormItemsMap } from "./config-screen-form-items-map";
import { ConfigScreenEnum } from "./constants";
import DateTimeIntervalPicker from "./date-time-picker";
import { usePreventNavigationLeave } from "./hooks/usePreventNavigationLeave";
import { ConfigFormItemsType } from "./types";
import ValidateContext from "./validate-context";
import ValidateContextProvider from "./validate-context-provider";
import ValidatedInput from "./validated-input";

type ConfigScreenProps = AuthStackScreenProps<AuthStackScreens.ConfigScreen>;
/**
 *
 * @param param0 object containing navigation information
 * @returns ConfigScreen object
 */
export default function ConfigScreen({ navigation }: ConfigScreenProps) {
  const { config, setConfig } = useContext(ConfigContext)!;
  const { expoPushToken } = useContext(AppContext)!;
  const [isChanged, setIsChanged] = useState(false);
  const [state, setState] = useState<ConfigObjectType>(config);

  const { buildKiviciUrl } = useBuildKiviciUrl();

  const isIos = Platform.OS === "ios";

  usePreventNavigationLeave({ isChanged, navigation, state });

  /**
   *
   * @param value value to change
   * @param key which key is connected to value
   */
  function handleChange(value: any, key: string | null) {
    if (!key) return;
    const _newState = { ...state, [key]: value };
    setState(_newState);
    setIsChanged(true);
    if (shallowCompare(_newState, config)) {
      setIsChanged(false);
    }
  }
  /**
   * Send notification to backend about configuration change
   */
  async function handleSubmit() {
    await setConfig({ ...state });
    await Axios.post(buildKiviciUrl("/clientConfig/register"), {
      clientToken: expoPushToken,
      clientConfigList: { ...state },
    });
    navigation.navigate(AuthStackScreens.OverviewScreen);
    setIsChanged(false);
  }
  /**
   * Send backend info whether user wants to receive notifications or not
   * @param value boolean if notifications should be sent or not
   */
  async function handleNotificationEnable(value: boolean) {
    await setConfig({ ...state, notificationEnable: value });
    await Axios.post(buildKiviciUrl("/clientConfig/enable"), { clientToken: expoPushToken, notificationEnable: value });
    setIsChanged(false);
  }

  /**
   *
   * @param param0 object containing parameters on which it is decided which components to render
   * @returns component for ConfigScreen
   */
  function renderInput({ key, label, type }: ConfigFormItemsType) {
    let _value: string = "";
    if (key != null) _value = String(state[key]);
    if (type === ConfigScreenEnum.HEADERS)
      return (
        <ListItem key={label} itemDivider first>
          <Text style={{ fontWeight: "bold" }}>{label}</Text>
        </ListItem>
      );
    if (type === ConfigScreenEnum.NOTIFICATION_HEADER) {
      return (
        <ListItem key={label} itemDivider first>
          <Left>
            <Text style={{ fontWeight: "bold" }}>{label}</Text>
          </Left>
          <Right style={{ minWidth: "30%" }}>
            {state.notificationEnable && (
              <Button
                small
                success
                onPress={() => {
                  handleChange(false, key);
                  handleNotificationEnable(false);
                  Toast.show({
                    text: "Notifications disabled",
                    position: "bottom",
                    type: "warning",
                  });
                }}>
                <Icon name="notifications-on" type="MaterialIcons" />
              </Button>
            )}
            {!state.notificationEnable && (
              <Button
                small
                warning
                onPress={() => {
                  handleChange(true, key);
                  handleNotificationEnable(true);
                  Toast.show({
                    text: "Notifications enabled",
                    position: "bottom",
                    type: "success",
                  });
                }}>
                <Icon name="notifications-off" type="MaterialIcons" />
              </Button>
            )}
          </Right>
        </ListItem>
      );
    }
    if (type === "NUMBER")
      return (
        <ValidatedInput
          key={key}
          inputKey={key}
          label={label}
          value={_value}
          onChange={(value) => handleChange(value, key)}
          onValidate={(value) => value && Number.isInteger(Number(value))}
          isChanged={isChanged}
        />
      );
    if (type === "IP_ADDRESS")
      return (
        <ValidatedInput
          key={key}
          inputKey={key}
          label={label}
          value={_value}
          onChange={(value) => handleChange(value, key)}
          onValidate={validateIPAddress}
          isChanged={isChanged}
        />
      );

    return (
      <AppInputWrapper label={label}>
        {type === "TEXT" && <Input value={_value} onChange={(e) => handleChange(e.nativeEvent.text, key)} />}
        {type === "PASSWORD" && <Input secureTextEntry value={_value} onChange={(e) => handleChange(e.nativeEvent.text, key)} />}
        {type === "SWITCH" && (
          <Switch
            style={{ marginTop: isIos ? 4 : 0, marginBottom: isIos ? 4 : -0 }}
            value={state[key]}
            onChange={(e: any) => handleChange(e?.nativeEvent?.value, key)}
          />
        )}
        {type === "DATE_TIME" && <DateTimeIntervalPicker value={state[key]} onChange={(value) => handleChange(value, key)} />}
      </AppInputWrapper>
    );
  }

  return (
    <Container>
      <ValidateContextProvider>
        <ValidateContext.Consumer>
          {(validateContext) => (
            <Content>
              <OverviewHeader
                navigation={navigation}
                title="Configuration"
                rightToolbar={
                  <Right>
                    {(!validateContext?.isAllValid || !isChanged) && (
                      <Button small block warning disabled>
                        <Icon name="save" type="MaterialIcons" />
                        <Text>Save</Text>
                      </Button>
                    )}
                    {validateContext?.isAllValid && isChanged && (
                      <Button small block onPress={handleSubmit} success>
                        <Icon name="save" type="MaterialIcons" />
                        <Text>Save</Text>
                      </Button>
                    )}
                  </Right>
                }
              />
              <Form>{ConfigScreenFormItemsMap?.map((item) => renderInput(item))}</Form>
            </Content>
          )}
        </ValidateContext.Consumer>
      </ValidateContextProvider>
    </Container>
  );
}
