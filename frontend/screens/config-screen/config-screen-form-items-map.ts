import { ConfigScreenEnum } from "./constants";
import { ConfigFormItemsType } from "./types";
/**
 * Map for config screen displaying
 */
export const ConfigScreenFormItemsMap: ConfigFormItemsType[] = [
  { key: null, label: "Icinga connection settings", type: ConfigScreenEnum.HEADERS },
  { key: "host", label: "Host", type: ConfigScreenEnum.IP_ADDRESS },
  { key: "port", label: "Port", type: ConfigScreenEnum.NUMBER },
  { key: "username", label: "Username", type: ConfigScreenEnum.TEXT },
  { key: "password", label: "Password", type: ConfigScreenEnum.PASSWORD },
  { key: "notificationEnable", label: "Notification settings", type: ConfigScreenEnum.NOTIFICATION_HEADER },
  { key: "notificationTimeInterval", label: "Activate [from - to]", type: ConfigScreenEnum.DATE_TIME, props: { mode: "time" } },
  { key: "event_CheckResult", label: "Check Result", type: ConfigScreenEnum.SWITCH },
  { key: "event_StateChange", label: "State Change", type: ConfigScreenEnum.SWITCH },
  { key: "event_AcknowledgementSet", label: "Ack. Set", type: ConfigScreenEnum.SWITCH },
  { key: "event_AcknowledgementCleared", label: "Ack. Cleared	", type: ConfigScreenEnum.SWITCH },
  { key: "event_CommentAdded", label: "Comment Added	", type: ConfigScreenEnum.SWITCH },
  { key: "event_CommentRemoved", label: "Comment Removed	", type: ConfigScreenEnum.SWITCH },
  { key: "event_DowntimeAdded", label: "Downtime Added	", type: ConfigScreenEnum.SWITCH },
  { key: "event_DowntimeRemoved", label: "Downtime Removed	", type: ConfigScreenEnum.SWITCH },
  { key: "event_DowntimeStarted", label: "Downtime Started	", type: ConfigScreenEnum.SWITCH },
  { key: "event_DowntimeTriggered", label: "Downtime Triggered	", type: ConfigScreenEnum.SWITCH },
  { key: "event_Flapping", label: "Flapping", type: ConfigScreenEnum.SWITCH },
];
