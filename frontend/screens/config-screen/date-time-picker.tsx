import DateTimePicker from "@react-native-community/datetimepicker";
import moment from "moment";
import { Text, View } from "native-base";
import React, { useState } from "react";
import { Platform, StyleSheet } from "react-native";

interface Props {
  value: Array<string>;
  onChange(value: Array<string>): any;
}
/**
 * 
 * @param props props for displaying picker
 * @returns DateTimeIntervalPicker
 */
const DateTimeIntervalPicker: React.FC<Props> = (props) => {
  const [from, setFrom] = useState(props?.value ? new Date(props?.value[0]) : new Date());
  const [to, setTo] = useState(props?.value ? new Date(props?.value[1]) : new Date());
  const [isFromShown, setIsFromShown] = useState(false);
  const [isToShown, setIsToShown] = useState(false);

  const isIos = Platform.OS === "ios";
  /**
   * 
   * @param value new value to change
   * @param key key to change
   * @returns null
   */
  function onChange(value: string, key: string) {
    let _value = moment(value).toISOString();
    if (!value) {
      setIsFromShown(false);
      setIsToShown(false);
      return;
    }
    if (key === "from") {
      setIsFromShown(false);
      if (moment(_value).isAfter(to)) _value = moment(_value).add(1, "day").toISOString();
      setFrom(new Date(_value));
      props.onChange([_value, moment(to).toISOString()]);
    }
    if (key === "to") {
      setIsToShown(false);
      if (moment(_value).isBefore(from)) _value = moment(_value).add(1, "day").toISOString();
      setTo(new Date(_value));
      props.onChange([moment(from).toISOString(), _value]);
    }
    console.log({ from: moment(from).format("LLL"), to: moment(to).format("LLL"), [key + "_"]: moment(_value).format("LLL") });
  }
  return (
    <>
      <View style={styles.container}>
        {(isIos ? !isFromShown && !isToShown : true) && (
          <>
            <Text style={styles.datePickerViewStyle} onPress={() => !isFromShown && setIsFromShown(true)}>
              {moment(from).format("HH:mm")}
            </Text>
            <Text style={styles.datePickerViewStyle}>-</Text>
            <Text style={styles.datePickerViewStyle} onPress={() => !isToShown && setIsToShown(true)}>
              {moment(to).format("HH:mm")}
            </Text>
          </>
        )}

        {(isFromShown || isToShown) && (
          <DateTimePicker
            value={isFromShown ? from : to}
            onChange={(e: any, value: any) => onChange(value, isFromShown ? "from" : "to")}
            mode="time"
            display={isIos ? "spinner" : "clock"}
            style={{ flex: 1 }}
            is24Hour
          />
        )}
      </View>
    </>
  );
};
export default DateTimeIntervalPicker;

const styles = StyleSheet.create({
  container: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    width: 100,
    minHeight: 38,
    marginRight: 20,
  },
  datePickerViewStyle: {
    paddingTop: 8,
  },
});
