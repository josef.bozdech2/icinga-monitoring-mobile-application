import { View } from "native-base";
import React from "react";
import { KeyboardAvoidingView } from "react-native";
import BigList from "react-native-big-list";
import { SafeAreaView } from "react-native-safe-area-context";
import { RowObject } from "../../interfaces/icinga.interface";
import ServiceItem from "./item";

interface Props {
  data: Array<RowObject>;
  onRefresh?: () => {};
}
/**
 * 
 * @param param0 object containing data object and onRefresh function for instant refresh call from user
 * @returns List component of icinga2 objects
 */
export const List: React.FC<Props> = ({ data, onRefresh }) => {
  function _renderItem({ item }: { item: RowObject }) {
    return <ServiceItem row={item} />;
  }

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <KeyboardAvoidingView style={{ flex: 1 }}>
        <View style={{ minHeight: 700, minWidth: 500 }}>
          <BigList data={data} renderItem={_renderItem} itemHeight={80} keyExtractor={(item) => item.name} onRefresh={onRefresh} />
        </View>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
};

export default React.memo(List);
