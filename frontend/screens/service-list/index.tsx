import React, { useContext, useState } from "react";
import { Container, Spinner } from "native-base";
import { useDebouncedCallback } from "use-debounce";
import useData from "../../hooks/use-data";
import List from "../../components/big-list/list";
import AppContext from "../../contexts/app-context";
import { OverviewHeader } from "../../components/app-headers";
import { BigListFilterHeader } from "../../components/big-list/header";
import { RowObject } from "../../interfaces/icinga.interface";
import ConfigContext from "../../contexts/config-context";
import { ServiceStackScreenProps, ServiceStackScreens } from "../../navigators/service-stack/types";
/**
 *
 * @param param0 object containing route and navigation parameters
 * @returns ServiceList screen component
 */
type ServiceListScreenProps = ServiceStackScreenProps<ServiceStackScreens.ServiceList>;

export default function ServiceList({ route, navigation }: ServiceListScreenProps) {
  const appContext = useContext(AppContext)!;
  const { config } = useContext(ConfigContext)!;

  const [filterValue, setFilterValue] = useState<string | null>(route?.params?.filterValue ? route?.params?.filterValue : null);
  const [picker, setPicker] = useState("-1");

  const { data, isLoading, onRefresh, fetchData } = useData({
    config,
    url: "/icingaProxy/service/list",
    dtoIn: { expoPushToken: appContext?.expoPushToken, ...getSearchValue() },
    refreshInterval: 30000,
  });
  /**
   * setter function for filter
   * @param value new value to store in state
   */
  const handleFilterChange = (value: string | null) => {
    handleFilterChangeAsync(value);
    setFilterValue(value);
  };
  /**
   * debounce function for data fetching, once the filter is changed, the application waits for 300 ms and then it calls instantly for new data to show
   */
  const handleFilterChangeAsync = useDebouncedCallback((value: string | null) => {
    fetchData({ expoPushToken: appContext?.expoPushToken, ...getSearchValue(value) });
  }, 300);

  // const handleFilterChange = (value: string) => {
  //   fetchData({ expoPushToken: appContext.expoPushToken, ...getSearchValue(value) });
  //   setFilterValue(value);
  // };
  /**
   *
   * @param _filterValue getter for filter value
   * @returns
   */
  function getSearchValue(_filterValue = filterValue) {
    if (!_filterValue) return {};
    return { host: _filterValue };
  }

  if (isLoading)
    return (
      <Container>
        <OverviewHeader navigation={navigation} title="Service List" />
        <BigListFilterHeader {...{ objectType: "Service", handleFilterChange, filterValue, picker, handlePickerChange: setPicker }} />
        <Spinner />
      </Container>
    );

  const _data: RowObject[] = data?.results?.filter((item: RowObject) =>
    picker === "0"
      ? true
      : picker === "-1"
      ? item?.attrs?.state == "1.0" || item?.attrs?.state == "2.0" || item?.attrs?.state == "3.0"
      : item?.attrs?.state == picker
  );

  return (
    <Container>
      <OverviewHeader navigation={navigation} title="Service List" />

      <BigListFilterHeader {...{ objectType: "Service", handleFilterChange, filterValue, picker, handlePickerChange: setPicker }} />
      <List
        data={_data}
        onRefresh={onRefresh}
        onRowClick={(row: RowObject) => navigation.navigate(ServiceStackScreens.ServiceDetail, { rowId: row.name })}
      />
    </Container>
  );
}
