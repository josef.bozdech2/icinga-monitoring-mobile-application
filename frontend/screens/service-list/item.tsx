import React from "react";
import { Body, Text, ListItem, Left, Right, Icon } from "native-base";
import moment from "moment";
import { useNavigation } from "@react-navigation/native";
import { RowObject } from "../../interfaces/icinga.interface";
import { ServiceStackScreens } from "../../navigators/service-stack/types";

interface Props {
  row: RowObject;
}
/**
 * 
 * @param param0 object containing one icinga2 object
 * @returns ServiceItem component
 */
const ServiceItem: React.FC<Props> = ({ row }) => {
  const navigation = useNavigation();
  return (
    <ListItem key={row.name} thumbnail onPress={() => navigation.navigate(ServiceStackScreens.ServiceDetail, { rowId: row.name })}>
      <Left>
        {row.attrs.state != "0" ? (
          <Icon style={{ fontSize: 20, color: "red" }} name="remove-circle" type="MaterialIcons" />
        ) : (
          <Icon style={{ fontSize: 20, color: "green" }} name="check-circle" type="MaterialIcons" />
        )}
      </Left>
      <Body>
        <Text>{row.name}</Text>
        <Text note numberOfLines={1}>
          {row.type}
        </Text>
      </Body>
      <Right>
        <Text>{moment.unix(row.attrs.last_check).fromNow()}</Text>
      </Right>
    </ListItem>
  );
};

export default React.memo(
  ServiceItem,
  (prevProps, nextProps) => prevProps.row.name === nextProps.row.name && prevProps.row.attrs.last_check === nextProps.row.attrs.last_check
);
