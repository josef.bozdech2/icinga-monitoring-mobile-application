import React, { useContext } from "react";
import { List, Spinner, Container, Content } from "native-base";
import useData from "../../hooks/use-data";
import AppContext from "../../contexts/app-context";
import { DetailHeader } from "../../components/app-headers";

import SectionPluginOutput from "../../components/entity-sections/plugin-output";
import SectionProblemHandling from "../../components/entity-sections/problem-handling";
import SectionPerformanceData from "../../components/entity-sections/performance-data";
import SectionNotifications from "../../components/entity-sections/notifications";
import SectionCheckExecution from "../../components/entity-sections/check-execution";
import SectionExistingComments from "../../components/entity-sections/existing-comments";
import SectionScheduledDowntimes from "../../components/entity-sections/scheduled-downtimes";
import SectionMoveToServiceList from "../../components/entity-sections/move-to-service-list";
import RefreshControllerComponent from "../../components/refresh-controller-component";
import ConfigContext from "../../contexts/config-context";
import { HostAndServiceDetailUseDataProps } from "../service-detail";
import { RowObject } from "../../interfaces/icinga.interface";
import { HostStackScreenProps, HostStackScreens } from "../../navigators/host-stack/types";
/**
 * 
 * @param param0 object containing navigation and route params for navigation
 * @returns HostDetail screen component
 */
export interface SectionProps {
  data: RowObject;
  onRefresh: () => void;
}
type HostDetailScreenProps = HostStackScreenProps<HostStackScreens.HostDetail>;
export default function HostDetail({ navigation, route }: HostDetailScreenProps) {
  const appContext = useContext(AppContext)!;
  const { config } = useContext(ConfigContext)!;

  const { data, isLoading, onRefresh }: HostAndServiceDetailUseDataProps = useData({
    config,
    url: "/icingaProxy/host/detail",
    dtoIn: { expoPushToken: appContext?.expoPushToken, hostId: route?.params?.rowId },
    refreshInterval: 30000,
  });

  const _sectionProps: SectionProps = { data, onRefresh };

  return (
    <Container>
      <RefreshControllerComponent onRefresh={onRefresh}>
        <Content>
          <DetailHeader
            navigation={navigation}
            title="Host Detail"
            subtitle={route?.params?.rowId || ""}
            status={data?.attrs?.state}
            type="host"
          />
          {isLoading ? (
            <Spinner />
          ) : (
            <List>
              <SectionMoveToServiceList {..._sectionProps} />
              <SectionPluginOutput {..._sectionProps} />
              <SectionCheckExecution {..._sectionProps} />
              <SectionProblemHandling {..._sectionProps} />
              <SectionNotifications {..._sectionProps} />
              <SectionExistingComments {..._sectionProps} />
              <SectionScheduledDowntimes {..._sectionProps} />
              <SectionPerformanceData {..._sectionProps} />
            </List>
          )}
        </Content>
      </RefreshControllerComponent>
    </Container>
  );
}
