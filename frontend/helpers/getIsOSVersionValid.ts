import * as Device from "expo-device";

/**
 * Checks for OS version
 * @returns boolean
 */
export function getIsOSVersionValid() {
  const { osName, osVersion } = Device;
  const currentVersion = Number(String(osVersion).split(" ")[0]);
  if (osName === "Android" && currentVersion >= 6) {
    return true;
  }
  if ((osName === "iOS" || osName === "iPadOS") && currentVersion >= 12) {
    return true;
  }
  return false;
}
