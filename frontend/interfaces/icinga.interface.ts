export interface AttrsObject {
  __name: string;
  name: string;
  state: string;
  last_check: number;
  check_command?: string;
  last_check_result?: LastCheckResultObject;
  next_check: number;
  max_check_attempts?: number;
  host_name?: string;
  acknowledgement?: number;
  acknowledgement_expiry?: number;
  active?: boolean;
  address?: string;
  address6?: string;
  check_attempt?: number;
  check_interval?: number;
  check_period?: string;
  command_endpoint?: string;
  display_name?: string;
  downtime_depth?: string;
  enable_active_checks?: boolean;
  enable_event_handler?: boolean;
  enable_flapping?: boolean;
  enable_notifications?: boolean;
  enable_passive_checks?: boolean;
  enable_perfdata?: boolean;
  event_command?: string;
  flapping?: boolean;
  flapping_last_change?:  number;
  flapping_negative?: number;
  flapping_positive?: number;
  flapping_threshold?: number;
  force_next_check?: boolean;
  force_next_notification?: boolean;
  last_hard_state?: number;
  last_hard_state_change?: number;
  last_reachable?: boolean;
  last_state?: number;
  last_state_change?: number;
  last_state_down?: number;
  last_state_type?: number;
  last_state_unreachable?: number;
  last_state_up?: number;
  notes?: string;
  notes_url?: string;
  original_attributes?: OriginalAtributesObject;
  package?: string;
  paused?: boolean;
  retry_interval?: number;
  state_type?: number;
  templates?: Array<string>;
  type?: string;
  vars?: string;
  version?: number;
  volatile?: boolean;
  zone?: string;
}

interface OriginalAtributesObject{
  enable_flapping?: boolean;
}
export interface LastCheckResultObject {
  active?: boolean;
  check_source?: string;
  command?: Array<string>;
  execution_start?: number;
  execution_end?: number;
  exit_status?: number;
  output?: string;
  performance_data?: Array<string>;
  schedule_end?: number;
  schedule_start?: number;
  state?: number;
  type?: number;
  vars_after?: VarsAfterOrBeforeObject;
  vars_before?: VarsAfterOrBeforeObject;
}

interface VarsAfterOrBeforeObject {
  attempt?: number;
  reachable?: boolean;
  state?: number;
  state_type?: number;
}

export interface RowObject {
  attrs: AttrsObject;
  name: string;
  type: string;
  joins: { host?: { address?: string }};
}

export interface CommentAttrsObject {
  __name: string;
  active?: boolean;
  author: string;
  entry_time: number;
  entry_type?: number;
  expire_time?: number;
  host_name?: string;
  legacy_id?: number;
  name?: string;
  paused?: boolean;
  service_name?: string;
  text: string;
  type: string;
  version?: number;
}
export interface DowntimeObject {
  attrs: DowntimeAttrsObject;
}

export interface CommentObject {
  attrs: CommentAttrsObject;
}
export interface DowntimeAttrsObject {
  __name: string;
  active?: boolean;
  author: string;
  comment?: string;
  config_owner?: string;
  duration?: number;
  end_time?: number;
  entry_time: number;
  fixed?: boolean;
  ha_mode?: number;
  host_name: string;
  legacy_id?: number;
  name: number;
  paused?: boolean;
  scheduled_by?: string;
  service_name?: string;
  start_time?: number;
  trigger_time?: number;
  triggered_by?: string;
  triggers?: Array<string>;
  type: string;
  version?: number;
}