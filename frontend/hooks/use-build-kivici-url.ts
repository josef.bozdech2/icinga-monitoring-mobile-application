import { useContext } from "react";
import ServerContext from "../contexts/server-context";
/**
 * Hook for setting backend
 * @returns object containing buildKiviciUrl function
 */
export const useBuildKiviciUrl = () => {
  const { serverConfig } = useContext(ServerContext)!;

  const port = serverConfig.port ? ":" + serverConfig.port : "";
  const host = serverConfig.host;

  const buildKiviciUrl = (path: string) => {
    const a = `http://${host}${port}${path}`;
    console.log({ a });
    return a;
  };

  return { buildKiviciUrl };
};
