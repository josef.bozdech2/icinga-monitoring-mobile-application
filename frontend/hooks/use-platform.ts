import { Platform } from "react-native";

interface ReturnedValue {
  isIos: boolean;
}
/**
 * check what OS the app is run on
 * @returns object containing boolean
 */
const usePlatform = (): ReturnedValue => {
  const isIos: boolean = Platform.OS === "ios";
  return { isIos };
};

export default usePlatform;
