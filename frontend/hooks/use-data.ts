import axios from "axios";
import { useEffect, useRef, useState } from "react";
import { Alert } from "react-native";
import { ConfigObjectType } from "../contexts/config-context";
import { RowObject } from "../interfaces/icinga.interface";
import { useBuildKiviciUrl } from "./use-build-kivici-url";
/**
 *
 * @param props props that are passed to axios to receive new data
 * @returns object containing loading status and functions for refresh
 */
export interface UseDataObject {
  data: any;
  isLoading: boolean;
  onRefresh: (isForcedRefresh?: boolean) => {};
  fetchData: (_dtoIn: any, url?: string) => {};
}

export interface UseDataProps {
  dtoIn: any;
  config?: ConfigObjectType;
  refreshInterval?: number;
  url: string;
}
/**
 * 
 * @param props UseDataProps object
 * @returns useData function
 */
const useData = (props: UseDataProps): UseDataObject => {
  const [data, setData] = useState<Array<RowObject> | null>(null);
  const [isLoading, setIsLoading] = useState(true);
  const [autoFetchCount, setAutoFetchCount] = useState(1);
  const prevUrl = useRef<string>();
  const dtoIn = useRef();

  const { buildKiviciUrl } = useBuildKiviciUrl();

  useEffect(() => {
    if (props.dtoIn !== dtoIn.current) dtoIn.current = props.dtoIn;
  });

  async function fetchData(url: string, isAutomaticRefresh: boolean = true) {
    prevUrl.current = url;
    try {
      console.log("Test");
      if (!isAutomaticRefresh) setIsLoading(true);
      const _data = await axios.get(buildKiviciUrl(url), { params: dtoIn.current });

      if (!_data) throw new Error("Backend could not load any data. Please verify your configuration");
      setData(_data.data);

      if (!isAutomaticRefresh) setIsLoading(false);
    } catch (error) {
      Alert.alert("Data cannot be loaded", `Error occurred during loading ${buildKiviciUrl(url)}`);
    }
  }

  useEffect(() => {
    fetchData(props.url, false);
  }, [props.url]);

  useEffect(() => {
    if (props.refreshInterval) {
      const timeout = setTimeout(async () => {
        await fetchData(props.url);
        setAutoFetchCount(autoFetchCount + 1);
      }, props.refreshInterval);
      return () => clearInterval(timeout);
    }
  }, [autoFetchCount]);

  return {
    isLoading,
    data,
    fetchData: async (_dtoIn, _url: string = props.url) => {
      if (_dtoIn) dtoIn.current = _dtoIn;
      return fetchData(_url);
    },
    onRefresh: async (isForcedRefresh: boolean = false) => {
      await fetchData(props.url, isForcedRefresh);
    },
  };
};

export default useData;
