import React, { useEffect, useContext } from "react";
import Constants from "expo-constants";
import * as Notifications from "expo-notifications";
import { Alert, Platform } from "react-native";
import AppContext from "../contexts/app-context";

Notifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: true,
    shouldSetBadge: true,
  }),
});
/**
 * if device doesn’t have expoPushToken, register for one
 * @returns object containing expoPushToken
 */
const useNotifications = (): { expoPushToken: string | null} => {
  const { expoPushToken, setExpoPushToken } = useContext(AppContext)!;

  useEffect(() => {
    if (!expoPushToken) registerForPushNotificationsAsync().then((token) => setExpoPushToken(token));
  }, []);
  return { expoPushToken };
};

export default useNotifications;
/**
 * Registering for notification token
 * @returns expoPushToken or uniqueToken
 */
async function registerForPushNotificationsAsync(): Promise<string> {
  let token:string;
  if (Constants.isDevice) {
    const { status: existingStatus } = await Notifications.getPermissionsAsync();
    let finalStatus = existingStatus;
    if (existingStatus !== 'granted') {
      const { status } = await Notifications.requestPermissionsAsync();
      finalStatus = status;
    }
    if (finalStatus !== 'granted') {
      Alert.alert("Failed to get push token for push notification!");
      return uniqueID();
    }
    token = (await Notifications.getExpoPushTokenAsync()).data;
    console.log(token);
  } else {
    return uniqueID();
  }

  if (Platform.OS === 'android') {
    Notifications.setNotificationChannelAsync('default', {
      name: 'default',
      importance: Notifications.AndroidImportance.MAX,
      vibrationPattern: [0, 250, 250, 250],
      lightColor: '#FF231F7C',
    });
  }

  return token;
}
/**
 * If application runs on an emulator or simulator, generate unique id for communication with backend
 * @returns Non-expo token
 */
function uniqueID() {
  return `MockPushToken=[${Math.floor(Math.random() * Date.now())}]`;
}
