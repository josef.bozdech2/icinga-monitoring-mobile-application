import Axios from "axios";
import React, { useEffect, useContext } from "react";
import AppContext from "../contexts/app-context";
import ConfigContext from "../contexts/config-context";
import { useBuildKiviciUrl } from "./use-build-kivici-url";
/**
 * Register for data from backend
 * @param expoPushToken device token
 */
export const useTokenRegistration = (expoPushToken: string | null) => {
  const { config } = useContext(ConfigContext)!;
  const { isSubscribed, setIsSubscribed } = useContext(AppContext)!;
  const { buildKiviciUrl } = useBuildKiviciUrl();

  useEffect(() => {
    async function useNotificationSubscription() {
      await Axios.post(buildKiviciUrl("/clientConfig/register"), {
        clientToken: expoPushToken,
        clientConfigList: config,
      }).then(() => setIsSubscribed(true));
    }
    if (expoPushToken && !isSubscribed) {
      console.log(`=========Sending expoPushToken ${expoPushToken}===========`);
      useNotificationSubscription();
    }
  }, [expoPushToken]);
};
