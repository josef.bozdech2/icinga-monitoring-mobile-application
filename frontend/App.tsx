import React, { useEffect, useState } from "react";
import { NavigationContainer } from "@react-navigation/native";
import * as Font from "expo-font";
import { Ionicons } from "@expo/vector-icons";
import { Container, Header, Root, Spinner, Toast } from "native-base";
import * as Updates from "expo-updates";
import ConfigContextProvider from "./contexts/config-context-provider";
import AppContextProvider from "./contexts/app-context-provider";
import { RootStackNavigator } from "./navigators/root-stack/root-stack";
import ServerContextProvider from "./contexts/server-context-provider";
/**
 * @author Josef Bozděch
 * @description Mobile application for viewing of hosts and services for Icinga2 monitoring system
 * @param param0 object containing navigation
 * @returns Main component
 */
export default function App({ navigation }: any) {
  const [isLoading, setIsLoading] = useState(true);

  useEffect((): void => {
    async function loadFonts() {
      await Font.loadAsync({
        Roboto: require("native-base/Fonts/Roboto.ttf"),
        Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
        ...Ionicons.font,
      });

      Updates.addListener(() => {
        Toast.show({
          text: "Nová akutalizace je k dispozici",
          position: "bottom",
          type: "warning",
          buttonText: "Update",
          duration: 30000,
          onClose: (reason) => reason === "user" && Updates.reloadAsync(),
        });
      });

      setIsLoading(false);
    }
    loadFonts();
  });

  if (isLoading)
    return (
      <Container>
        <Header />
        <Spinner />
      </Container>
    );

  return (
    <AppContextProvider>
      <ServerContextProvider>
        <ConfigContextProvider>
          <NavigationContainer>
            <Root>
              <RootStackNavigator />
            </Root>
          </NavigationContainer>
        </ConfigContextProvider>
      </ServerContextProvider>
    </AppContextProvider>
  );
}
